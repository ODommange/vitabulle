<?php
namespace includes;

use includes\Request;
use includes\Login;
use includes\Bootstrap;
use includes\Adm;

use stdClass;

class Template{

    private static $_userInfos      = null;
    
    public static function page( $pageInfos )
    {
        if( ( $pageInfos[ 'page' ] === 'login' && $pageInfos[ 'action' ] === 'disconnect' ) )                                // 2. User Disconnect or visitor url access attempt not allowed
        {
            Login::logout();
        }
        else if( $pageInfos[ 'page' ] === 'login' && $pageInfos[ 'action' ] === 'newpass' )
        {
            Login::passrecovery();
        }
        else if( $pageInfos[ 'page' ] === 'login' && $pageInfos[ 'action' ] === 'changepass' )
        {
            Login::passchange();
        }
        else 
        {
           self::_render('page', $pageInfos );

        }
    }

    private static function _includeInTemplate( $page, $action = '', $router = '' )
    { 
        if( $page === 'home' )
        {     
            self::_render( 'pages/'.$page );
        }
        else if( $page === 'page' && $action === 'topmenu' )
        {     
            self::_render( 'topmenu', Login::isLoguedIn() );
        }
        else if( $page === 'page' && $action === '404' )
        {     
            self::_render( '404' );
        }
        else
        {
            if(file_exists(SITE_PATH.'/applications/'.$page.'/Controller.php'))
            {
                include_once SITE_PATH.'/applications/'.$page.'/Controller.php';

                $controllerPath = '\applications\\'.$page.'\Controller';
                $controller = new $controllerPath( $page, $action, $router );

                self::_render('pages/'.$controller->view(), $controller->datas());
            }
            else
            {
                self::_render( 'pages/'.$page);
            }
        }
    }


    private static function _render( $view, $datas = '' )
    {
        
        $user = self::$_userInfos;
        
        // Effectue les rendus pour l'affichage à l'aide de la mémoire tampon
        ob_start();
        
                include SITE_PATH . '/views/' . $view . '.php'; 
                $template = ob_get_contents();
                
        ob_end_clean();

        echo $template;
    }
    
   
}

	