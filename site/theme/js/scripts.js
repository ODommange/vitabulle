$(document).ready(function () {


    $('.menu').on('click',function(){
        
        $('nav').slideToggle();
        
        $('.menu > div').toggleClass("blanc");
        
        $('nav ul li').slideToggle();
        
    });
    
    
    $('.affiche').on('click',function(){
        
        $(".cache").animate({ height: 'toggle'});
        
    });
    

    $('a.anchor').on('click', function(e){
        
        e.preventDefault();
        
        var currentPage = window.location.pathname;
        
        var currentURLParts = currentPage.split('/');
        
        if( currentURLParts[( currentURLParts.length - 1)].length > 0 && currentURLParts[( currentURLParts.length - 1)] !== 'home' )
        {
            window.location = "" + $(this).attr('href');
        }
        else
        {        
            var currentHash = $(this).attr('href').split('#');
            
            scrollToContent( '#' + currentHash[( currentHash.length - 1 )] );
        }
        
    });


    var accessContent = location.hash;
    
    if( accessContent.length > 0 )
    {
        $('html, body').css({scrollTop:0});
        
        scrollToContent( accessContent );
    }
    
});



var scrollToContent = function( section ){
    
    $('.selected').removeClass('selected');
    
    var position =  $(section).offset().top;

    $('html, body').animate({scrollTop:position}, 1000, function(){
        
        $(section).addClass('selected');
        
    });
};

    
