<?php
namespace applications\users;

use includes\Login;
use stdClass;

include('ModelStats.php');

class Controller {

    private $_action;
    private $_page;
    private $_datas = '';
    private $_view;
    private $_router;
    
    public function __construct( $page, $action, $router )
    {
        $this->_page    = $page;
        $this->_action  = $action;
        $this->_router  = $router;
        $this->_setDatas();
    }


    private function _setDatas()
    {
        $modelStats = new ModelStats();

        switch( $this->_action ){
            
            case 'private':
                
                    if( $this->_datas = $modelStats->recoverPass( $this->_router ) )
                    {
                        $this->_view = 'profil-recoverpass';
                    }
                    else
                    {
                        header('location:' . SITE_URL );
                    }
                break;
            
            
            case 'validate':
                    $this->_datas = $modelStats->validateProfil( $this->_router );
                    $this->_view = 'validateprofil';
                break;
                
            case 'menu':
                $this->_datas = $modelStats->getCurrentMenu();
                $this->_view = 'profil-menu';
                break;
                
            case 'profil':
                if( Login::isLoguedIn() )
                {
                    $this->_datas = $modelStats->getProfilDatas();
                    $this->_view = 'profil';
                }
                else
                {
                    header('location:' . SITE_URL );
                }
                break;
                
                
            case 'profilform':
                if( Login::isLoguedIn() )
                {
                    $this->_datas = $modelStats->getProfilDatas();
                    $this->_view = 'profil-form';
                }
                else
                {
                    header('location:' . SITE_URL );
                }
                break;

                
            case 'profilUpdate':
                if( Login::isLoguedIn() )
                {
                    $updateProfil = new ModelStats();

                    if( $updateProfil->updateProfil( $this->_router ) ) 
                    {
                        header('location:' . SITE_URL . '/users/profil');
                        exit;
                    }
                    else
                    {
                        $this->_datas = $modelStats->getProfilDatas();
                        $this->_view = 'profil-form';
                    }
                }
                else
                {
                    header('location:' . SITE_URL );
                }
                break;

                
            case 'results':
                if( Login::isLoguedIn() )
                {
                    $modelStats->getStats();
                    $this->_datas = $modelStats->multiDatas(($this->_router));
                    $this->_view = 'results';
                }
                else
                {
                    header('location:' . SITE_URL );
                }
                break;

                
            case 'results-day':
                if( Login::isLoguedIn() )
                {
                    $this->_datas = $modelStats->getResDay(($this->_router));
                    $this->_view = 'results-day';
                }
                else
                {
                    header('location:' . SITE_URL );
                }
                break;

                
            case 'login':
                if( Login::isLoguedIn() )
                {
                    header('location:' . SITE_URL . '/users/profil');
                    exit;
                }
                else
                {
                    $login = Login::doLogin();
                    if( !$login )
                    {
                        $this->_datas = new stdClass();
                        $this->_datas->error = 'La connexion n\'a pas pu se faire. Veuillez essayer de nouveau.';
                    }
                    $this->_view = 'users';
                }
                break;

                
            case 'recoverpasssend':
                
                $verdict = Login::passrecovery();
         
                $this->_datas = $verdict;
                $this->_view = 'recoverpass';
        
                break;
                
            default :
                $this->_view = 'users';
                break;
        }
    }

    public function datas() {
        return $this->_datas;
    }

    public function view() {
        return $this->_view;
    }

}
