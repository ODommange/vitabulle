<?php

namespace applications\users;

use includes\tools\Orm;
use includes\Db;
use includes\Login;
use includes\Request;
use includes\Bootstrap;
use stdClass;

class ModelStats {

    private $_days = ['lu', 'ma', 'me', 'je', 've', 'sa', 'di'];
    private $_monthFr = ['Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucric;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre'];
    private $_day;
    private $_month;
    private $_year;
    private $_startTime = 0;
    private $_endTime = 0;
    private $_arrayDateAndNbrExercice = [];
    
    private $_mapUsers = ['users' => [
            'IdUser'        => [ 'type' => 'INT', 'primary' => true, 'autoincrement' => true, 'dependencies' => ['users_statistics' => 'IdUser']],
            'PseudoUser'    => [ 'type' => 'STR', 'mandatory' => true],
            'PassUser'      => [ 'type' => 'STR'],
            'IdGroup'       => [ 'type' => 'INT', 'mandatory' => true],
            'LastnameUser'  => [ 'type' => 'STR'],
            'FirstnameUser' => [ 'type' => 'STR'],
            'BirthdayUser'  => [ 'type' => 'DATE', 'dateformat' => 'DD.MM.YYYY'],
            'EmailUser'     => [ 'type' => 'STR'],
            'PhoneUser'     => [ 'type' => 'STR'],
            'AddressUser'   => [ 'type' => 'STR'],
            'ZipCodeUser'   => [ 'type' => 'STR'],
            'CityUser'      => [ 'type' => 'STR'],
            'ParamsUser'    => [ 'type' => 'STR'],
            'TokenUser'     => [ 'type' => 'STR'],
            'IsAccountActivated' => [ 'type' => 'INT'],
    ]];
    private $_mapUsersStats = ['users_statistics' => [
            'IdStatistic' => ['type' => 'INT', 'primary' => true, 'autoincrement' => true, 'dependencies' => ['table' => 'IdField']],
            'IdUser' => [ 'type' => 'INT', 'mandatory' => false],
            'ValueStatistic' => [ 'type' => 'INT', 'mandatory' => false],
            'DateStatistic' => [ 'type' => 'DATE', 'default' => 'NOW', 'dateformat' => 'DD.MM.YYYY',],
    ]];
    
    private $_menuRoute = [ 
        'profil'    => [ 'routes' => ['profil', 'login'],            'url' => '/users/profil',      'name' => 'Mon profil',             'selected' => true ],
        'results'   => [ 'routes' => ['results', 'results-day'], 'url' => '/users/results',     'name' => 'Mes résultats',          'selected' => false ],
        'form'      => [ 'routes' => ['profilform', 'profilUpdate'],     'url' => '/users/profilform',  'name' => 'Modifier mon profil',    'selected' => false ] 
    ];


    
    public function getCurrentMenu()
    {
        $action = Bootstrap::$action;
        
        $menuSelected = '';
        
        foreach( $this->_menuRoute as $m => $menus )
        {
            if( isset( $menus[ 'routes' ] ) && is_array( $menus[ 'routes' ] ) )
            {
                foreach( $menus[ 'routes' ] as $menu )
                {
                    if( $menu === $action )
                    {
                        $menuSelected = $m;
                        
                        $this->_menuRoute[ $m ][ 'selected' ] = true;
                    }
                }
            }
        }
        
        if( $menuSelected !== 'profil' )
        {
            $this->_menuRoute[ 'profil' ][ 'selected' ] = false;
        }
        
        return $this->_menuRoute;
    }
    
    /**
     * get the user's profile datas in DB
     * @return Object
     */
    public function getProfilDatas()
    {
        $orm = new Orm('users', $this->_mapUsers['users']);

        $res = $orm->build(['IdUser' => $_SESSION['IdUser']]);

        return $res;
    }
    
    
    public function recoverPass( $urlRouter )
    {
        $urlInfos = explode( '-', $urlRouter );
                
        if( count( $urlInfos ) === 2 )
        {
            $orm = new Orm('users', $this->_mapUsers['users']);

            $res = $orm->select()->where([ 'TokenUser' => $urlInfos[1] ])->first();

            if( isset( $res ) )
            {
                $datas = new stdClass();
                
                $datas->displayform = true;
                
                $req = Request::getInstance();
                
                if( $req->getVar( 'password1' ) !== null && $req->getVar( 'password2' ) !== null )
                {
                    $datas->verdict = Login::passchange();
                    
                    if( !isset( $datas->verdict['status'] ) || $datas->verdict['status'] === 'OK' )
                    {
                        $datas->displayform = false;
                    }
                }
                
                $datas->formurl = SITE_URL . '/users/private/' . $urlRouter;
                
                return $datas;
            }
            return false;
        }
        
        return null;
    }
    
    public function validateProfil( $urlRouter )
    {
        $urlInfos = explode( '/', $urlRouter );
        
        if( count( $urlInfos ) === 2 && !empty( $urlInfos[0] ) && !empty( $urlInfos[1] ) )
        {
            $orm = new Orm('users', $this->_mapUsers['users']);

            $res = $orm->select()->where(['IdUser' => $urlInfos[0], 'TokenUser' => $urlInfos[1]])->first();
            
            if( isset( $res ) )
            {              
                $orm->prepareDatas([ 'IsAccountActivated' => '1' ]);
                
                $orm->update(['IdUser' => $urlInfos[0] ]);

                return $res;
            }
        }
        
        return false;
    }
    
    /**
     * update the user's profile in DB
     * @param type $IdUser
     * @return boolean | header('location:' . SITE_URL . '/users/profil');
     */
    public function updateProfil( $IdUser )
    {
        $orm = new Orm( 'users', $this->_mapUsers[ 'users' ] );

        $orm->prepareGlobalDatas( [ 'POST' => true ] );

        if( !$orm->issetErrors() )
        {
            $orm->update( [ 'IdUser' => $IdUser ] );
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * get the user's statistics in DB
     * @return type Object
     */
    public function getStats() {

        $orm = new Orm('users_statistics', $this->_mapUsersStats['users_statistics']);

        $res = $orm->build(['IdUser' => $_SESSION['IdUser']]);

        return $res;
    }

    /**
     * 
     * @param str $dateMonth Date format mm-yyyy
     * @return type
     */
    public function multiDatas($dateMonth = '')
    {
        if( empty( $dateMonth ) )
        {
            $month          = intval(date('m'));
            $year           = intval(date('Y'));
            $this->_month    = $month;
            $this->_year     = $year;
        }
        else 
        {
            $this->_dateMonth    = $dateMonth;
            $datesArrayMonth    = explode('-', $dateMonth);
            $this->_month        = $datesArrayMonth[0];
            $this->_year         = $datesArrayMonth[1];
        }

        $datastab = [];

        $datastab['getWeeks']   = $this->getWeeks();
        $datastab['day']        = $this->_day;
        $datastab['month']      = $this->_month;
        $datastab['year']       = $this->_year;
        $datastab['daysweek']   = $this->_days;
        $datastab['timestamp']  = mktime( 0, 0, 0, $this->_month, 1, $this->_year );

        $datastab['firstdayweek'] = date( 'N', $datastab['timestamp'] );

        $datastab['timestamplastday'] = mktime(0, 0, 0, ( $this->_month + 1), 0, $this->_year);
        $datastab['lastdayofmonth'] = date('j', $datastab['timestamplastday']);


        $datastab['nextMonth']  = $this->nextMonth();
        $datastab['previousMonth'] = $this->previousMonth();

        $datastab['objectCalendar'] = $this->getDateAndNbrEx();
        //$datastab['activeBtn'] = $this->activeBtn();
        
        return $datastab;
    }

    /**
     * Renvoie le nombre de semaine dans le mois courant
     * @return int
     */
    public function getWeeks()
    {
        $timeStart          = mktime( 0, 0, 0, $this->_month, 1, $this->_year );
        $firstDayInMonth    = date( 'Y-m-d', $timeStart );
        $date               = new \DateTime( $firstDayInMonth );
        $startWeek          = $date->format( "W" );


        $lastDayInMonth     = date('t', $timeStart);

        $timeEnd            = mktime(0, 0, 0, $this->_month, $lastDayInMonth, $this->_year);
        $end                = date('Y-m-d', $timeEnd);
        $date               = new \DateTime($end);
        $endWeek            = $date->format("W");

        $weeks = $endWeek - $startWeek + 1;

        if( $weeks < 0 )
        {
            $weeks = intval( $date->format( 'W' ) );
        }
        return $weeks;
    }

    /**
     * Permet d'incrementer le mois 
     * @return stdClass Object 
     */
    public function nextMonth()
    {
        $month  = $this->_month;
        $year   = $this->_year;

        if( $month >= 12 )
        {
            $month = 1;
            //echo ($this->_month);
            $year += 1;
        }
        else
        {
            $month = $this->_month + 1;
        }

        $nextMonth = new stdClass();

        $nextMonth->nextMonth = $month;
        $nextMonth->nextYear = $year;

        return $nextMonth;
    }

    /**
     * Permet decrementer le mois 
     * @return stdClass Object
     */
    public function previousMonth()
    {
        $month = $this->_month - 1;
        $year = $this->_year;

        if( $month < 1 )
        {
            $month = 12;
            $year -= 1;
        }

        $previousMonth = new stdClass();

        $previousMonth->previousMonth = $month;
        $previousMonth->previousYear = $year;

        return $previousMonth;
    }

    /**
     * int $this->_month
     * int $time format (1541458800)
     * int year format (2018)
     * string $month format (November)
     * Revoie un tableau qui contient la date ex: (1) et le nombre d'exercice 
     * @return stdClass Object
     */
    public function getDateAndNbrEx() {
        $db = DB::db();

        $dateObject = new stdClass();

        $time = mktime( 0, 0, 0, $this->_month );
        $month = date('F', $time);
        $year = $this->_year;

        $dateObject->month = $this->_monthFr[ ( $this->_month - 1 ) ];
        //$dateObject->month = $month;
        $dateObject->year = $year;
        // start date = 1
        $start_date = mktime(0, 0, 0, $this->_month, 1, $this->_year);
        $start_date = date('01', $start_date);

        // start_date_month_name = November
        $start_date_month_name = mktime(0, 0, 0, ( $this->_month), 1, $this->_year);
        $start_date_month_name = date('F', $start_date_month_name);

        // end date retourne le dernier jour du mois courant
        $end_date = mktime(0, 0, 0, ( $this->_month), 1, $this->_year);
        $end_date = date('t', $end_date);

        // convert to int 
        $start_date = intval($start_date);
        $end_date = intval($end_date);

        $dateObject->start_day = $start_date;
        $dateObject->end_day = $end_date;

        for( $i = $start_date; $i <= $end_date; $i++ )
        {
            if( $i === 1 )
            {
                $this->_startTime = mktime(0, 0, 0, ( $this->_month), 1, $this->_year);
                $this->_endTime = mktime(23, 59, 59, ( $this->_month), 1, $this->_year);

                $date_1 = date('Y-m-d', $this->_startTime);
                $date_2 = date('Y-m-d', $this->_endTime);
            }
            else
            {
                $date_1 = $this->_startTime + 86400;
                $date_2 = $this->_startTime + 86400;

                $this->_startTime = $date_1;
                $this->_endTime = $date_2;

                $date_1 = date('Y-m-d', $date_1);
                $date_2 = date('Y-m-d', $date_2);
            }

            if( isset( $_SESSION[ 'IdUser' ] ) ) 
            {
                $IdUser = $_SESSION['IdUser'];
            }
            
            $results = $db->query("select count(*) DateStatistic  FROM  users_statistics WHERE DateStatistic BETWEEN '" . $date_1 . " 00:00:00'"
                    . " and " . "'" . $date_2 . " 23:59:59'"
                    . " and IdUser='" . $IdUser . "'"
            );

            if( $results->num_rows > 0 )
            {
                // output data of each row
                while( $row = $results->fetch_assoc() )
                {
                    // i = first day in month between las day in month
                    $keys = $i;
                
                    $this->_arrayDateAndNbrExercice[$keys] = $row['DateStatistic'];
                }
            }
        }

        $dateObject->daysOfMont = $this->_arrayDateAndNbrExercice;

        return $dateObject;
    }

    /**
     * 
     * @param type $date format 17/10/2018
     * @return type array
     */
    public function getResDay($date) {
        $db = DB::db();

        $datastab = [];

        $dateArray      = explode('/', $date);

        $this->_day      = $dateArray[0];
        intval($this->_day);
        $this->_month    = $dateArray[1];
        $this->_year     = $dateArray[2];

        $datastab['day'] = $this->_day;
        $datastab['monthFr'] = $this->_monthFr[ ( $this->_month - 1 ) ];
        $datastab['month'] = $this->_month;
        $datastab['year'] = $this->_year;
        $datastab['resQuery'] = [];

        $datastab['nextDay'] = $this->nextDay();
        $datastab['previousDay'] = $this->previousDay();

        if( isset( $_SESSION['IdUser'] ) )
        {
            $IdUser = $_SESSION['IdUser'];
        }

        $results = $db->query("select * FROM  users_statistics WHERE DateStatistic BETWEEN '" . $this->_year . "-" . $this->_month . "-" . $this->_day . " 00:00:00'"
                . " and " . "'" . $this->_year . "-" . $this->_month . "-" . $this->_day . " 23:59:59'"
                . " and IdUser='" . $IdUser . "'"
        );

        $datastab['nbExercices'] = $results->num_rows;
        
        if( $results->num_rows > 0 )
        {
            // output data of each row
            while( $row = $results->fetch_assoc() )
            {
                array_push( $datastab['resQuery'], $row );
            }
        }

        return $datastab;
    }

    /**
     * permet d'incrementer le jour
     * @return stdClass Object
     */
    public function nextDay()
    {
        $time = mktime(0, 0, 0, $this->_month, $this->_day, $this->_year);
        $date = intval(date('t', $time));

        $day = intval($this->_day);
        $year = intval($this->_year);
        $month = intval($this->_month);

        if( $day < $date )
        {
            $day = $this->_day + 1;
        }
        else
        {
            $day = 1;
            $month = $this->_month + 1;
        }

        while ($month > 12) {
            $month = 1;
            $year += 1;
        }

        $nextDay = new stdClass();
        $nextDay->nextDay = $day;
        $nextDay->nextMonth = $month;
        $nextDay->nextYear = $year;

        return $nextDay;
    }

    /**
     * Permet decrementer le jour
     * @return stdClass Object
     */
    public function previousDay()
    {
        $day    = intval( $this->_day );
        $year   = intval( $this->_year );
        $month  = intval( $this->_month );


        if( $day <= 1 )
        {
            $time = mktime(0, 0, 0, $this->_month - 1, $this->_day, $this->_year);
            $date = intval(date('t', $time));
            $day = $date;
            $month = $this->_month - 1;
        }
        else
        {
            $day = $this->_day - 1;
        }

        while( $month < 1 )
        {
            $month = 12;
            $year -= 1;
        }

        $previousDay                = new stdClass();
        $previousDay->previousDay   = $day;
        $previousDay->previousMonth = $month;
        $previousDay->previousYear  = $year;

        return $previousDay;
    }
}
