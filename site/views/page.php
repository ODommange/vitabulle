<!DOCTYPE html>
<html>
<head>
    <title>Vitabulle</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo SITE_URL; ?>/theme/images/favicon.png" rel="shortcut icon" type="image/png" />
    <script src="<?php echo SITE_URL; ?>/theme/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo SITE_URL; ?>/theme/js/scripts.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>/theme/css/styles.css">
</head>
<body>
    <div class="container">
        <header>
            <a class="logo" href="<?php echo SITE_URL; ?>"><img src="<?php echo SITE_URL; ?>/theme/images/logo-vitabulle.svg"></a>
            <div class= "menu">
                <div class="barre1"></div>
                <div class="barre2"></div>
                <div class="barre3"></div>
            </div>
            <nav> 
                <?php self::_includeInTemplate( 'page', 'topmenu' ); ?>
            </nav>
        </header>
        
        <main role="main" class="<?php echo $datas['page']; ?>">
            <?php self::_includeInTemplate( $datas['page'], $datas['action'], $datas['router'] ); ?>
        </main>
    </div>
    
    <footer>
        <div class="container">
            <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-9">
         <ul>
             <li><hr></li>
             <li><strong>Editeur</strong></li>
             <li><a href="http://www.lausanne.ch">Ville de Lausanne</a></li>
             <li><a href="http://www.emploilausanne.ch">Emploi Lausanne</a></li>
             <li><a href="http://www.emploilausanne.ch">5D</a></li>
        </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-9">
        <ul>
             <li><hr></li>
             <li><strong>Information</strong></li>
             <li><a href="<?php echo SITE_URL; ?>/conditions">Conditions d’utilisation</a></li>
             
        </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-9">
        <ul>
             <li><hr></li>
             <li><strong>Vitabulle</strong></li>
             <li><a href="home#effets" class="anchor">Effets bénéfiques</a></li>
             <li><a href="home#guide" class="anchor">Guide rapide</a></li>
             <li><a href="home#app" class="anchor">Téléchargement</a></li>
        </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-9">
        <ul>
             <li><hr></li>
             <li><strong>Contact</strong></li>
             <li><a href="http://www.emploilausanne.ch">emploilausanne@lausanne.ch</a></li>      
        </ul>
         </div>
            </div>
     </div>

    </footer>
</body>
</html>
