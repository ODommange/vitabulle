<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/chapeau.png">
        </div>
        <div class="col-lg-7 col-sm-9">



            <h1 class="page">Amélioration de la concentration et de la mémorisation</h1>

                    <p>
                        La difficulté de se concentrer dans un quotidien empli de stress, d'obligations et autres stimulations incessantes est le mal du siècle. Le manque de concentration ne concerne pas que les enfants et les adolescents, mais chacun d'entre nous. L’impression, avérée ou seulement ressentie, de souffrir de troubles de la mémoire, peut aussi provenir d’une trop grande tension due au stress.
                    </p>
                    <p>
                       De nombreux professionnels de la santé font état de nettes améliorations dans la concentration et la mémoire, lorsque sont pratiqués les exercices de Cohérence cardiaque.
                        Voici l’un d’eux. 
                        On parle beaucoup du TDHA ou TDAH, alias Troubles du déficit d'attention avec ou sans hyperactivité. Pour certains thérapeutes, la Cohérence cardiaque est une méthode qui permet d'améliorer la concentration et donc de diminuer ces troubles, ainsi qu'expliqué <a href="http://deficit-attention.com/coherence-cardiaque-concentration/" class="vert"> ici.</a>
                    </p>
                    <p>
                        Un autre professionnel en matière de concentration vante la Cohérence cardiaque pour booster sa mémoire. Il s’agit de  <a href="https://www.sebastien-martinez.com/blog/ameliorer-concentration-grace-a-coherence-cardiaque/" class="vert">Sébastien Martinez.</a>  Formateur en mémorisation et champion de France de mémoire, Sébastien Martinez explique comment «améliorer sa concentration grâce à la Cohérence cardiaque».

                    </p>




            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">Consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>