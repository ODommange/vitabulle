<main>

    <section id="video">
        <div class="row">
            <div class="col-lg-6 col-sm-9">
                <h1>Retrouvez calme et sérénité <br>en seulement 5 minutes!</h1>
                 <h3>Avec l'application gratuite Vitabulle, redécouvrez une véritable sensation de bien-être en pratiquant la Cohérence cardiaque assistée.
                 <a class="center" href="<?php echo SITE_URL; ?>/5minutes"><br>En savoir plus ></a></h3>
            </div>
            <div class="col-lg-6 col-sm-9">
                <video class="play" controls poster="<?php echo SITE_URL; ?>/theme/medias/poster.jpg">
                    <source src="<?php echo SITE_URL; ?>/theme/medias/vitabulle.mp4" type="video/mp4">
                </video>
                <!--<img class="play" src="<?php echo SITE_URL; ?>/theme/images/video.jpg">-->
            </div>
            <div class="col-lg-6 col-sm-9">
               
            </div>
        </div>
    </section>



    <section id="app">
        <div class="row">
            <div class="col-lg-6 col-sm-9">
                <h3>Assistant gratuit utile à la pratique de la Cohérence cardiaque.
                <br>Disponible en application Web ou Android (installation manuelle).</h3>
                
            </div>
            
            <div class="col-lg-3 col-md-6 col-sm-9 ">
                <a href="<?php echo SITE_URL; ?>/../app" class="bouton center">
                    <div class="bouton-icone"><img src="<?php echo SITE_URL; ?>/theme/images/pwa-icon.svg"></div>
                    <div class="bouton-text">accéder à la web app</div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-9 ">
                <a href="<?php echo SITE_URL; ?>/theme/app/vitabulle.apk" class="bouton center">
                    <div class="bouton-icone"><img src="<?php echo SITE_URL; ?>/theme/images/icone-android.svg"></div>
                    <div class="bouton-text">télécharger l'app gratuitement</div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-9">
                <div class="logo-box">
                    <img class="" src="<?php echo SITE_URL; ?>/theme/images/logo-app.svg">
                    <img class="logotype" src="<?php echo SITE_URL; ?>/theme/images/logotype.svg">
                </div>
            </div>
        </div>
    </section>


    <section id="effets">
        <h1>Offrez à votre corps les bienfaits d'un simple
            <br> exercice de respiration reconnu.</h1>
        <div class="lead">
            Cette technique de respiration vous aidera à retrouver calme
            <br> et sérénité en seulement trois fois 5 minutes par jour.
            <br> Et une pratique régulière sur le long terme vous apportera
            <br>encore bien plus d’effets bénéfiques...
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-9 card center"><a class=" center" href="<?php echo SITE_URL; ?>/stress">
                <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/arbre.png">
                <div class="titre">
                    <h3>Diminution du stress</h3>
                    <p>Impression générale de calme grâce à la baisse du cortisol, sécrétée lors d'un état de tension.</p>
                    <div class="plus">En savoir plus ></div>
                </div>
            </a>
            </div>
            
            <div class="col-lg-4 col-md-6 col-sm-9 card center"><a class=" center" href="<?php echo SITE_URL; ?>/vieillissement">
                <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/sablier.png">
                <div class="titre">
                    <h3>Ralentissement du vieillissement</h3>
                    <p>Augmente la DHEA, l'hormone dite “de la jeunesse”.</p>
                    <div class="plus">En savoir plus ></div>
                </div>
            </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-9 card center"><a class=" center" href="<?php echo SITE_URL; ?>/renforcement">
                <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/bouclier.png">
                <div class="titre">
                    <h3>Renforcement immunitaire</h3>
                    <p>Augmente le taux d'immunoglobulines A (IgA), renforçant la défense immunitaire.</p>
                    <div class="plus">En savoir plus ></div>
                </div>
            </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-9 card center"><a class=" center" href="<?php echo SITE_URL; ?>/memoire">
                <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/chapeau.png">
                <div class="titre">
                    <h3>Amélioration de la concentration et de la mémorisation</h3>
                    <p>Diminution des troubles de l’attention et de l’hyperactivité.</p>
                    <div class="plus">En savoir plus ></div>
                </div>
            </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-9 card center"><a class=" center" href="<?php echo SITE_URL; ?>/douleur">
                <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/choc.png">
                <div class="titre">
                    <h3>Meilleure tolérance<br> à la douleur</h3>
                    <p>Atténue les maux et les souffrances liées à certaines maladies</p>
                    <div class="plus">En savoir plus ></div>
                </div>
             </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-9 card center"><a class=" center" href="<?php echo SITE_URL; ?>/coeur">
                <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/coeur.png">
                <div class="titre">
                    <h3>Ouverture du coeur</h3>
                    <p>Augmente la sécrétion d’ocytocine, appelée aussi “hormone de l’amour”</p>
                    <div class="plus">En savoir plus ></div>
                </div>
            </a>
            </div>
            <div class="cache">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-9 card center"><a class=" center" href="<?php echo SITE_URL; ?>/sucre">
                        <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/sucres.png">
                        <div class="titre">
                            <h3>Meilleure régulation<br> du taux de sucre</h3>
                            <p>Trop de glucose dans le sang peut engendrer des troubles cardio-vasculaires</p>
                            <div class="plus">En savoir plus ></div>
                        </div>
                    </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-9 card center "><a class=" center" href="<?php echo SITE_URL; ?>/recuperation">
                        <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/chaussure.png">
                        <div class="titre">
                            <h3>Meilleure récupération après l’effort</h3>
                            <p>Améliore la vitalité et l'endurance lors de la pratique d'une activité physique</p>
                            <div class="plus">En savoir plus ></div>
                        </div>
                    </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-9 card center "><a class=" center" href="<?php echo SITE_URL; ?>/diminution">
                        <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/cardiogramme.png">
                        <div class="titre">
                            <h3>Diminution de l’hypertension artérielle</h3>
                            <p>Amélioration de la résistance face aux risques cardio-vasculaires</p>
                            <div class="plus">En savoir plus ></div>
                        </div>
                    </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-9 center affiche">
                <div class="bouton">
                    <div class="bouton-text">Afficher plus</div>
                </div>
            </div>
        </div>
    </section>


    <section id="guide">
        <div class="row">
            <div class="col-lg-6 col-sm-9 ">
                <h2>La Cohérence cardiaque,<br> une technique de respiration reconnue</h2>
                <div class="icone-box">
                    <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/serpent.png">
                    <div class="titre">
                        <h3 class="gris">Découvrez l’origine de la cohérence cardiaque <a class=" center" href="<?php echo SITE_URL; ?>/cardiaque">ici.</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-9" id="video2">
                <iframe class="play" src="https://www.youtube.com/embed/QkOCO30Uw1c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
                <div class="col-lg-6 col-md-12 col-sm-9">
                    <p>La Cohérence cardiaque,<br> par le Dr Jean-Pierre Houppe, Cardiologue à Thionville (F)</p>
                </div>
            </div>
        </div>
    </section>


    <section id="guide">
        <div class="row">
            <div class="col-lg-6 col-sm-9">
                <h2>Guide rapide avec l’app Vitabulle</h2>
                <div class="icone-box">
                    <h3>Choisissez un endroit calme.</h3>
                </div>
                <div class="icone-box">
                    <h3>Prenez une position assise ou restez debout.</h3>
                </div>
                <div class="icone-box">
                        <h3>Respirez et inspirez - idéalement 6 fois (4 à 7 fois) - par minute pendant 5 minutes en vous laissant guider par le visuel et l’audio de l'app.</h3>
                </div>
                <div class="icone-box">
                    <h3>Répétez la séance 3 fois par jour à des moments opportuns.
                        <ul>
                            <li>Au lever (si possible avant le petit déjeuner).</li>
                            <li>Avant le repas de midi.</li>
                            <li>A l’heure du goûter ou avant le repas du soir.</li>
                            <li>Une heure avant le coucher (optionnel).</li>
                        </ul>                        
                    </h3>
                </div>
                
                <div class="row">
                
                <p>Votre santé n'a pas de prix, cette application non plus!
                    Alors profitez de vous faire du bien. La Ville de Lausanne (Suisse)
                    et Emplois Lausanne (5D) ont le plaisir de vous offrir cette application.
                    Téléchargez-la et commencez la cohérence cardiaque dès aujourd’hui.
                </p>
                    <div class="col-md-6 col-sm-9 ">
                        <a href="<?php echo SITE_URL; ?>/../app" class="bouton center">
                            <div class="bouton-icone"><img src="<?php echo SITE_URL; ?>/theme/images/pwa-icon.svg"></div>
                            <div class="bouton-text">accéder à la web app</div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-sm-9">
                        <a href="<?php echo SITE_URL; ?>/theme/app/vitabulle.apk" class="bouton center">
                            <div class="bouton-icone"><img src="<?php echo SITE_URL; ?>/theme/images/icone-android.svg"></div>
                            <div class="bouton-text">télécharger l'app gratuitement</div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-sm-9">
                        <div class="logo-box">
                            <img class="" src="<?php echo SITE_URL; ?>/theme/images/logo-app.svg">
                            <img class="logotype" src="<?php echo SITE_URL; ?>/theme/images/logotype.svg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-9 " id="video3">
                <figure>
                    <img class="play" src="<?php echo SITE_URL; ?>/theme/images/teaser.jpg">
                    <figcaption><p>Aperçu de l’application</p></figcaption>
                </figure>
            </div>
        </div>
    </section>
</main>