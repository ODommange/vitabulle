<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/coeur.png">
        </div>
        <div class="col-lg-7 col-sm-9">
            
            
            
            <h1 class="page">Ouverture du coeur</h1>

            <div class="lead">Augmente la sécrétion d’ocytocine, appelée aussi «hormone de l’amour»</div>
            
                    <p>
                        Pour les pionniers de la Cohérence cardiaque que sont David Servan-Schreiber et David O'HARE, les émotions sont en lien avec les battements du cœur.
                    </p>
                    <p>
                        Appelée aussi l’hormone de l’amour, ou de l’attachement, ou encore de la confiance,  <a href="https://fr.wikipedia.org/wiki/Ocytocine">l’ocytocine</a>, secrétée par des neurones, est étudiée par les scientifiques pour son rôle présumé dans les relations humaines. Publié dans la <em>Revue médicale suisse</em> en 2012, cet  <a href="https://www.revmed.ch/RMS/2012/RMS-333/L-ocytocine-hormone-de-l-amour-de-la-confiance-et-du-lien-conjugal-et-social" class="vert">article</a> titré: «L’ocytocine: hormone de l’amour, de la confiance et du lien conjugal et social» en dit plus sur cette molécule.
                    </p>
                    <p>
                        <a href="https://coherence-coeur.com/la-coherence-cardiaque-pour-les-debutants/">L’Institut Heartmath</a>, spécialisé sur le sujet, l’affirme: «Quand nous éprouvons authentiquement  des émotions comme l’appréciation, la joie, l’intérêt pour l’autre et l’amour, notre rythme cardiaque devient très     ordonné, cohérent. La courbe est lisse, formée d’ondes harmonieuses. C’est ce qu’on appelle la Cohérence cardiaque.»
                        Le Dr David O'Hare assure que la pratique de la Cohérence cardiaque augmente la sécrétion d’ocytocine. Et il explique comment dans son livre  <a href="https://www.thierrysouccar.com/bien-etre/livre/coherence-cardiaque-365-107" class="vert">Cohérence cardiaque 365</a>  (Ed. Thierry Souccar): «Appelée aussi l’hormone de l’amour, ou de l’attachement, l’ocytocine est un neurotransmetteur d’information émotionnelle très curieux. Elle favorise l’attachement et a été appelée hormone de l’amour. Elle est particulièrement sécrétée par les femmes qui accouchent et qui allaitent et elle semble renforcer le lien avec leur enfant. La sécrétion accrue d’ocytocine par la Cohérence cardiaque procure du plaisir à être en présence de personnes aimées. Avoir du cœur, avoir le cœur sur la main, avoir un grand cœur sont des expressions populaires ancestrales qui trouvent ainsi un écho biologique».

                    </p>
                    <p>
                        Pour comprendre les mécanismes de l’ocytocine, surnommée ici «l’hormone du câlin», regardez cette conférence (sous-titrée en français) donnée par la psychologue Kelly McGonial et présentée sur la plateforme  <a href="https://www.ted.com/" class="vert">Ted.com.</a> Elle propose  <a href="https://www.youtube.com/watch?v=OXEITtka68c" class="vert">«une façon complètement nouvelle de réfléchir au stress».</a> Son credo? Cesser de craindre le stress mais plutôt en faire un allié. Et  <a href="http://kellymcgonigal.com/" class="vert">la thérapeute</a> américaine de préciser: «On peut modifier notre perception du stress en réalisant que l’ocytocine, libérée lors d’une réponse à une situation stressante, se comporte comme un anti-inflammatoire naturel. Via cette hormone, le stress nous donne accès à nos cœurs.»
                    </p>
            
            

            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">Consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>