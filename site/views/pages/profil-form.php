<div class="row">
    <div class="col-lg-3 col-sm-9">
        <?php self::_includeInTemplate( 'users', 'menu' ); ?>
    </div>
    <div class="col-lg-9 col-sm-9">

        <form method="post" action="<?php echo SITE_URL; ?>/users/profilUpdate/<?php echo $datas->IdUser ?>">
            <?php if( isset( $datas->errors[ 'PseudoUser' ] ) ) { ?>
                <span>Veuillez remplir ce champ !</span>
            <?php } ?>


            <label for="lastnameuser">Nom de famille </label>
            <input id="lastnameuser" type="text" name="LastnameUser" value="<?php echo $datas->LastnameUser ?>">

            <label for="firstnameuser">Prénom </label>
            <input id="firstnameuser" type="text" name="FirstnameUser" value="<?php echo $datas->FirstnameUser ?>">

            <label for="email">Email / Nom d'utilisateur</label>
            <input id="email" type="email" name="EmailUser" disabled="disabled" value="<?php echo $datas->EmailUser ?>">

            <input id="birthdayuser" type="hidden" name="BirthdayUser" value="<?php echo $datas->BirthdayUser ?>">
            <input id="phoneuser" type="hidden" name="PhoneUser" value="<?php echo $datas->PhoneUser ?>">
            <input id="adresse" type="hidden" name="AddressUser" value="<?php echo $datas->AddressUser ?>">
            <input id="zipcode" type="hidden" name="ZipCodeUser" value="<?php echo $datas->ZipCodeUser ?>">
            <input id="cityuser" type="hidden" name="CityUser" value="<?php echo $datas->CityUser ?>">
            <input id="pseudouser" type="hidden" name="PseudoUser" value="<?php echo $datas->PseudoUser ?>">

            <button name="editbtn" type="submit">Enregistrer</button>

        </form>

    </div>
</div>