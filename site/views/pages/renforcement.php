<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/bouclier.png">
        </div>
        <div class="col-lg-7 col-sm-9">


            <h1 class="page">Renforcement immunitaire</h1>

            <div class="lead">Augmente le taux des immunoglobulines A (IgA), renforçant la défense immunitaire.</div>


                    <p>
                        Les immunoglobulines est le terme médical utilisé pour désigner les <a href="https://sante-medecine.journaldesfemmes.fr/faq/8106-anticorps-definition" class="vert">anticorps.</a> Les anticorps sont des agents essentiels de notre système de défense immunitaire.
                        Pour en connaître davantage sur les différents types d' immunoglobulines, rendez-vous, par exemple, sur le site de vulgarisation scientifique <a href="https://www.futura-sciences.com/sante/definitions/medecine-immunoglobuline-2431/" class="vert">Futura.</a>
                        Les immunoglobulines A (IgA) se trouvent dans diverses sécrétions organiques tels que le suc intestinal, la salive ou encore la sueur. Elles sont aussi présentes dans le lait maternel.

                    </p>
                    <p>
                        «La principale fonction des IgA (pour «immunoglobulines A») est de défendre les muqueuses (tissus mous tapissant l'intérieur des organes creux et des cavités naturelles de l'organisme) et l'épiderme contre les nombreux agents pathogènes (bactéries, virus…) existants. Le déficit en IgA est le plus courant des déficits immunitaires.» Tiré du site de santé <a href="https://sante-medecine.journaldesfemmes.fr/faq/32852-iga-definition" class="vert">Journal des Femmes.</a>
                    </p>
                    <p>
                        Une personne en déficit d'«immunoglobulines A» est plus sujette aux infections du système digestif et respiratoire que la normale.

                    </p>
                    <p>
                        Les <a href="https://fr.wikipedia.org/wiki/Immunoglobuline_A" class="vert">immunoglobulines A</a> (IgA) sont un isotype d'anticorps majoritairement produit au niveau des muqueuses, où elles constituent une première ligne de défense immunitaire contre les toxines et les agents infectieux présents dans l'environnement.
                    </p>
                    <p>
                        Dans son livre «Cohérence cardiaque 365», le Dr David O'Hare, détaille <a href="https://www.thierrysouccar.com/bien-etre/info/les-effets-de-la-coherence-cardiaque-989" class="vert">l'effet de cette technique</a> de respiration sur la défense immunitaire.
                        «Augmentation des IgA salivaires (Immunoglobulines A), des facteurs qui participent à <a href="https://www.thierrysouccar.com/sante/info/comment-fonctionne-le-systeme-immunitaire-510" class="vert">la défense immunitaire.</a> C’est l’action de la Cohérence cardiaque sur les IgA qui explique une grande partie des effets de la Cohérence cardiaque sur le renforcement immunitaire." Extrait du livre «Cohérence cardiaque 365», paru aux Ed. Thierry Souccar en 2012.
                    </p>



            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>