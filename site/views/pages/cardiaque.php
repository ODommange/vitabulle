<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/serpent.png">
        </div>
        <div class="col-lg-7 col-sm-9">



            <h1 class="page">La Cohérence cardiaque, une technique de respiration reconnue</h1>


            <p>
                La technique de la Cohérence cardiaque (Cardiac Coherence) a vu le jour à peu près simultanément aux Etats-Unis, au Canada et en France, peu avant l’an 2000.
                L’idée d’influer, en respirant régulièrement, sur son rythme cardiaque, a donné lieu à une quantité de recherches médicales, engendrant cette méthode de respiration reconnue depuis une vingtaine d’années et appelée Cohérence cardiaque.

            </p>
            <p>
                Les bénéfices sur la santé de cette technique de respiration ont été décrits dans de multiples études et ont fait naître de nombreux livres. Parmi ceux-ci, citons les ouvrages de référence du Dr David Servan-Schreiber, «Guérir», dont le sous-titre est «Guérir le stress, l’anxiété, la dépression sans médicaments ni psychanalyse», <em>Ed. Robert Laffont</em>, 2003, et celui du Dr David O’Hare, « Cohérence cardiaque 365 », sous-titré «Guide de la cohérence cardiaque jour après jour », <em>Ed. Thierry Souccar</em>, 2008
            </p>
            <p>
                Devenue une méthode reconnue, la Cohérence cardiaque est appliquée et enseignée aujourd’hui dans de multiples hôpitaux, cliniques, centres de soin et autres instituts. Parmi ceux-ci, citons le HeartMath Institute, basé en Californie, dont la filiale à Montpellier (F) propose une vaste <a href="https://coherence-coeur.com/" class="vert">documentation en français</a> sur le sujet.
            </p>




            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">Consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>