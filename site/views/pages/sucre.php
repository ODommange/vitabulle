<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/sucres.png">
        </div>
        <div class="col-lg-7 col-sm-9">
            
            
            
            <h1 class="page">Meilleure régulation du taux de sucre</h1>

                    <p>
                        Chacun sait que taux de sucre dans le sang est néfaste s’il est trop élevé. Une présence élevée de glucose dans le corps peut favoriser les troubles cardio-vasculaires et «la contraction des vaisseaux sanguins peut augmenter le risque de crise cardiaque» (Lire <a href="https://www.pourquoidocteur.fr/Articles/Question-d-actu/13573-Crise-cardiaque-comment-le-sucre-augmente-les-risques" class="vert">ici </a>dans <a href="https://www.pourquoidocteur.fr/" class="vert"> Pourquoidocteur.fr</a>). 
                    </p>
                    <p>
                       Une meilleure régulation du taux de sucre est particulièrement indiquée chez les diabétiques de type 2, sujets entre autres à d'accidents neurologiques et circulatoires liés à cette maladie, insuffisamment diagnostiquée chez nous.
                    </p>
                    <p>
                        La technique de la Cohérence cardiaque permet une meilleure régulation du taux de sucre, ainsi que le prétend le Dr David O’Hare, dans ses ouvrages, conférences et sur son <a href="https://www.coherenceinfo.com/videotheque/videotheque-david-servan-schreiber/" class="vert">site</a> (coherenceinfo.com)
                    </p>
            
            
            
            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">Consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>
