<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/cardiogramme.png">
        </div>
        <div class="col-lg-7 col-sm-9">



            <h1 class="page">Retrouvez calme et sérénité en seulement 5 minutes!</h1>
            <div class="lead">Avec l'application gratuite Vitabulle, redécouvrez une véritable sensation de bien-être en pratiquant la cohérence cardiaque assistée.</div>

                    <p>
                        Le stress est le poison de note société. Il coûte des milliards aux services de santé et est responsable d’innombrables maladies, troubles cardiaques, notamment.

                    </p>
                    <p>
                        Avec l’application Vitabulle, vous pouvez apprendre à modérer votre stress, jusqu’à l’éliminer totalement.
                        Apprenez à maîtriser votre respiration et vous serez capable de retrouver calme et sérénité en seulement cinq minutes!

                    </p>
                    <p>
                        L’application gratuite Vitabulle vous aide à comprendre les bienfaits de la méthode de la Cohérence cardiaque, et vous assistera, pas à pas, afin que vous puissiez ensuite pratiquer en solitaire cette technique aux multiples bienfaits.
                    </p>
                    <p>
                        Avec l'application gratuite Vitabulle, vous retrouverez votre vitalité et redécouvrirez une véritable sensation de bien-être.
                    </p>
                    <p>
                        A raison de cinq minutes, trois fois par jour, vous allez vous isoler dans votre bulle, afin d’apprendre à respirer calmement et régulièrement. Le rythme proposé est de six respirations par minute, soit une inspiration et une expiration toutes les dix secondes. Les petites bulles de l’application Vitabulle vous permettront de garder ce rythme tout au long des cinq minutes de l’exercice.
                    </p>
                    <p>
                        Le nombre 365 est fréquemment associé à la pratique de la Cohérence cardiaque.<br>
                        En effet, <br>
                        Trois fois par jour<br>
                        Six respirations par minute<br>
                        Cinq minutes durant<br>
                        Et vous pratiquez ce même exercice tout au long des 365 jours de l’année.<br>

                    </p>




            <h1 class="page">Offrez à votre corps les bienfaits d'un simple exercice de respiration reconnu. </h1>
            <div class="lead">Après seulement quelques séances, cette technique médicale vous aidera à retrouver calme et sérénité en seulement trois fois 5 minutes par jour.</div>

                    <p>
                        Offrez à votre corps les bienfaits d'un simple exercice de respiration reconnu.
                    </p>
                    <p>
                        Cet exercice de respiration, issu de la méthode dite de la Cohérence cardiaque, offre de multiples bienfaits pour la santé. Ceux-ci ont été largement étudiés par des thérapeutes et décrits par de nombreux auteurs (médecins, psychologues, professionnels de la santé, coachs etc.), différents centres de soin et autres instituts depuis quelques années. Voici de quoi en savoir davantage sur ces bienfaits.

                    </p>
                    <p>
                        Les effets positifs de cette technique sur la santé sont détaillés sur <a href="http://www.coherence-cardiaque.com/decouvrir_effets_cc.html" class="vert">ce site de référence</a> en français.

                    </p>


            

            <h1 class="page">Une pratique régulière sur le long terme vous apportera encore davantage d’effets bénéfiques...</h1>
                    
                    <p>
                        En France voisine, Les Thermes d’Allevard proposent à leurs curistes, la technique de la cohérence cardiaque. 
                        Ils détaillent <a href="http://www.thermes-allevard.com/content/respirelax-0" class="vert">ici</a> cet état particulier de la fréquence cardiaque. «Un stade physiologique naturel dans lequel les systèmes - nerveux, cardio-vasculaire, hormonal et immunitaire - travaillent de manière efficiente et harmonieuse. Le cœur entre alors en «résonance» avec la respiration qui est forcée à 6 cycles complets par minute (inspiration puis expiration).
                        Mais dans les faits, chaque individu a sa propre fréquence respiratoire sur laquelle la Cohérence cardiaque est optimale. Cette fréquence se trouve entre 0,06 et 0,12 Hertz soit 4 à 7 respirations complètes par minute.



                    </p>
                    <p>
                        <b>A noter la mise en garde publiée sur leur site: «Depuis 2014, les cardiologues conseillent sa pratique régulière pour limiter les risques d’accidents cardio-vasculaires.»</b>
                    </p>



        </div>
    </div>
</section>