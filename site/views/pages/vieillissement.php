<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/sablier.png">
        </div>
        <div class="col-lg-7 col-sm-9">
            
            
            
            <h1 class="page">Ralentissement du vieillissement</h1>

            <div class="lead">Augmente la DHEA, l’hormone dite «de la jeunesse»</div>
            
                    <p>
                        Le Dr David O'Hare, spécialiste mondial de la Cohérence cardiaque, estime que cette méthode augmente également le taux de DHEA dans le sang. <a href="https://fr.wikipedia.org/wiki/Déhydroépiandrostérone">La déhydroépiandrostérone</a> (DHA ou DHEA) est <a href="https://fr.wikipedia.org/wiki/Androgène">un androgène</a> réputé pour ses effets <a href="https://fr.wikipedia.org/wiki/Vieillissement">antivieillissement .</a> Il est actuellement considéré comme<a href="https://fr.wikipedia.org/wiki/Dopage_(sport)"> un produit dopant</a> par <a href="https://fr.wikipedia.org/wiki/Agence_mondiale_antidopage">l'Agence mondiale antidopage.</a>
                    </p>
                    <p>
                        La production de DHEA fréquemment surnommée aussi «hormone de jouvence» décroît régulièrement avec l'âge.
                    </p>
                    <p>
                        Stimuler sa production en pratiquant, comme le préconisent divers thérapeutes, la cohérence cardiaque, permettrait donc de ralentir le vieillissement. Le Dr David O’Hare l'explique, dans <a href="https://www.thierrysouccar.com/bien-etre/livre/coherence-cardiaque-365-107">sa méthode</a>: «C’est l’action de la cohérence cardiaque sur la DHEA qui explique une grande partie des effets de la cohérence cardiaque sur le ralentissement du vieillissement.» Un récent article publié sur le site <a href="https://www.pressesante.com/coherence-cardiaque-dr-david-ohare-stresse/">PresseSanté.com</a> lui donne la parole: «La cohérence entre le cœur et le cerveau émotionnel stabilise le système nerveux autonome, l’équilibre sympathique/parasympathique s’installe. Une fois parvenu dans cet état, nous sommes dans une situation optimale pour faire face à toutes les éventualités. Dans cet état, on peut se débarrasser des palpitations, des attaques de panique, de l’anxiété. Des suivis rigoureux sur des cohortes de personnes à qui l’on a appris cette simple méthode respiratoire ont vu leur taux de DHEA (l’hormone de jouvence) augmenter de 100%. Le taux circulant de cortisol, l’hormone du stress par excellence associée aux poussées de tension artérielle, au vieillissement de la peau autant qu’à la perte de mémoire et de concentration a baissé de 25%.»
                    </p>

            
            
            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">Consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>