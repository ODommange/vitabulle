<section id="login">
    <form method="post" action="<?php echo SITE_URL; ?>/users/login">
        <h1>Gestion du profil</h1>
        <div class="lead">
            <p>Connectez-vous pour personnaliser votre profil et accéder aux résultats de vos exercices.</p>
            <p>La création d'un compte se fait depuis l'application.</p>
        </div>
        <?php if( isset( $datas->error ) ){ ?>
            <div class="msg msg-error"><?php echo $datas->error; ?></div>
        <?php } ?>
            
        <label for="adminuser">Adresse e-mail </label>
        <input type="text" name="adminuser" id="adminuser">
        
        <label for="adminpass">Mot de passe </label>
        <input type="password" name="adminpass" id="adminpass">
        <p><a href="<?php echo SITE_URL; ?>/recoverpass">Mot de passe perdu ?</a>
        <button>Se connecter</button>
    </form>
</section>