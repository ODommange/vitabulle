<div class="row">
    <div class="col-lg-3 col-sm-9">
        <?php self::_includeInTemplate( 'users', 'menu' ); ?>
    </div>
    <div class="col-lg-9 col-sm-9">
        <div>
            <a class="backBtn" href="<?php echo SITE_URL; ?>/users/results/<?php echo $datas['month']; ?>-<?php echo $datas['year']; ?>">
                <img src="<?php echo SITE_URL; ?>/theme/images/arrow-left.svg"><?php echo $datas['monthFr']; ?> <?php echo $datas['year']; ?>
            </a>
        </div>
        
        <table class="calendar-title">
            <tr id="cal_month">
                <th colspan="2">
                    <a href="<?php echo SITE_URL; ?>/users/results-day/<?php echo $datas['previousDay']->previousDay; ?>/<?php echo $datas['previousDay']->previousMonth; ?>/<?php echo $datas['previousDay']->previousYear; ?>">&lt;</a>
                    <h4><?php echo $datas['day']; ?> <?php echo $datas['monthFr']; ?> <?php echo $datas['year']; ?></h4>
                    <a href="<?php echo SITE_URL; ?>/users/results-day/<?php echo $datas['nextDay']->nextDay; ?>/<?php echo $datas['nextDay']->nextMonth; ?>/<?php echo $datas['nextDay']->nextYear; ?>">&gt;</a>
                </th>
            </tr>
            <?php if( $datas['nbExercices'] > 0 ){ ?>
                <tr class="cal_d_week">
                    <th colspan="2"><?php echo $datas['nbExercices'] . ' exercices réalisés' ?></th>
                </tr>
            <?php } ?>
            
        </table>
        
        <table class="calendar">
            <?php if( $datas['nbExercices'] > 0 ){ ?>
                <?php foreach( $datas['resQuery'] as $k => $day ) { ?>
                <tr>
                    <td><?php 
                    $date = explode( ' ', $day['DateStatistic'] ); 
                    $time = explode( ':', $date[1] );
                    echo $time[0] .  ':' . $time[1]; ?></td>
                    <td><img src="<?php echo SITE_URL; ?>/theme/images/eval_0<?php echo $day['ValueStatistic']; ?>.png"></td>
                </tr>
                <?php } ?>
            <?php }else{ ?>
                <tr>
                    <td colspan="2"><h4 class="gray">Aucune pratique recensée ce jour.</h4></td>
                </tr>
            <?php } ?>
        </table>
        
    </div>
</div>

