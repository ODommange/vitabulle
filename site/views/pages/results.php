<div class="row">
    <div class="col-lg-3 col-sm-9">
        <?php self::_includeInTemplate( 'users', 'menu' ); ?>
    </div>
    <div class="col-lg-9 col-sm-9">
        <table class="calendar-title">
            <tr id="cal_month">
                <th colspan="7">
                <a href="<?php echo SITE_URL; ?>/users/results/<?php echo $datas['previousMonth']->previousMonth; ?>-<?php echo $datas['previousMonth']->previousYear; ?>">&lt;</a>
                <h4><?php echo $datas['objectCalendar']->month; ?> <?php echo $datas['objectCalendar']->year; ?></h4>
                <a href="<?php echo SITE_URL; ?>/users/results/<?php echo $datas['nextMonth']->nextMonth; ?>-<?php echo $datas['nextMonth']->nextYear; ?>">&gt;</a>
                </th>
            </tr>
        </table>
        
        <table class="calendar">
            <tr class="cal_d_week">
            <?php foreach( $datas['daysweek'] as $k => $day ){ ?>

                <th><?php echo $day; ?></th>

            <?php } ?>
            </tr>
            <?php for( $i = 0; $i < 6; $i++ ){ ?>
                <tr>
                    <?php foreach( $datas['daysweek'] as $k => $day ){ ?>
                        <td>
                            <?php if( ( $i === 0 && $datas['firstdayweek'] <= ( $k + 1 ) ) || ( $i > 0 && $datas['objectCalendar']->start_day <= $datas['objectCalendar']->end_day ) ){ ?>
                                <a href="<?php echo SITE_URL; ?>/users/results-day/<?php echo $datas['objectCalendar']->start_day; ?>/<?php echo $datas['month']; ?>/<?php echo $datas['year']; ?>">
                                <?php echo $datas['objectCalendar']->start_day; ?>
                                <span>
                                    <?php 
                                    $daysOfMonth = $datas['objectCalendar']->daysOfMont[ $datas['objectCalendar']->start_day ];
                                    echo ( empty( $daysOfMonth ) || $daysOfMonth === 0 ) ? '' : $daysOfMonth; 
                                    ?>
                                </span>
                                </a>
                                <?php $datas['objectCalendar']->start_day++; ?>
                            <?php } ?>
                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>