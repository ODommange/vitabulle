<section id="login">
    <form method="post" action="<?php echo SITE_URL; ?>/users/recoverpasssend">
        <h1>Gestion du profil</h1>
        <div class="lead">
            <p>La récupération d'un mot de passe se fait par l'envoi d'un e-mail dans ta messagerie afin d'authentifier ton inscription à Vitabulle.</p>
        </div>
        <?php if( isset( $datas['status'] ) ){ ?>
            <?php if( $datas['status'] === 'FAIL' ){ 
                foreach( $datas['errors'] as $error ){ ?>
                <div class="msg msg-error"><?php echo $error; ?></div>
                <?php } ?>
            <?php } ?>
            <?php if( $datas['status'] === 'OK' ){ ?>
                <div class="msg msg-success">Un message te permettant de définir un mot de passe vient de t'être envoyé par e-mail.</div>
            <?php } ?>
        <?php } ?>
            
        <label for="adminemailrecover">Adresse e-mail </label>
        <input type="text" name="adminemailrecover" id="adminemailrecover">

        <button>Envoyer un message</button>
    </form>
</section>