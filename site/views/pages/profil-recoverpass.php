<section id="login">
    <form method="post" action="<?php echo $datas->formurl; ?>">
        <h1>Récupération du mot de passe</h1>
        <div class="lead">
            <p>Tu peux définir un nouveau mot de passe.</p>
        </div>
        <?php if( isset( $datas->verdict['status'] ) ){ ?>
            <?php if( $datas->verdict['status'] === 'FAIL' ){ ?>
                <div class="msg msg-error"><?php echo $datas->verdict['errors']; ?></div>
            <?php } ?>
            <?php if( $datas->verdict['status'] === 'OK' ){ ?>
                <div class="msg msg-success">
                    Le mot de passe que tu as défini est enregistré. Tu peux tenter de te connecter de nouveau.
                </div>
            <?php } ?>
        <?php } ?>
            
        <?php if( isset( $datas->displayform ) && $datas->displayform ){ ?>
            <label for="adminemailrecover">Nouveau mot de passe </label>
            <input type="password" name="password1" id="adminemailrecover">

            <label for="adminemailrecover">Validation du mot de passe </label>
            <input type="password" name="password2" id="adminemailrecover">
            
        <?php } ?>
        
        <input type="hidden" name="token">

        <button>Enregistrer</button>
    </form>
</section>