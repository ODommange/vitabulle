<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            
        </div>
        <div class="col-lg-7 col-sm-9">
            
            <?php if( $datas ){ ?>
            
                <h1 class="page">Votre compte est activé</h1>

                <p>
                    Vous pouvez dès à présent profiter de l'application et de son support à la cohérence cardiaque en vous connectant sur votre application mobile.
                </p>
            
            <?php } else { ?>
            
                <h1 class="page">Votre authentification n'a pas pu s'effectuer</h1>

                <p>
                    Vous pouvez tenter de nouveau en indiquant votre adresse e-mail.
                </p>
                
                <form method="post" action="<?php echo SITE_URL; ?>/users/profilSendActivate">
                    <?php if (isset($datas->errors['EmailUser'])) { ?>

                        <span>Veuillez remplir ce champ !! </span>
                        <?php
                    }
                    ?>
                    <label for="email">Email </label>
                    <input id="email" type="email" name="EmailUser" value="">
                    <button name="submitbtn" type='submit' >Envoyer</button>
                </form>

            <?php } ?>
        </div>
    </div>
</section>