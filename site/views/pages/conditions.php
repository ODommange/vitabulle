<h1>Conditions générales d'utilisation de l'application et du site Vitabulle</h1>
<h2>ARTICLE 1: Objet</h2>
<p>
Les présentes «conditions générales d'utilisation» ont pour objet l'encadrement juridique des modalités de mise à disposition des services de l'application et du site Vitabulle et leur utilisation par «l'Utilisateur».
</p>
<p>
Les conditions générales d'utilisation doivent être acceptées par tout Utilisateur souhaitant utiliser l'application et accéder au site. Elles constituent le contrat entre l'application, le site et l'Utilisateur. L’accès à l'application ou au site par l’Utilisateur signifie son acceptation des présentes conditions générales d’utilisation.
</p>
<ul>
    <li>En cas de non-acceptation des conditions générales d'utilisation stipulées dans le présent contrat, l'Utilisateur se doit de renoncer à l'accès des services proposés par l'application et le site.</li>

    <li>L'application et du site Vitabulle se réservent le droit de modifier unilatéralement et à tout moment le contenu des présentes conditions générales d'utilisation.</li>
</ul>

<h2>ARTICLE 2: Mentions légales</h2>
<p>
L'édition de l'application et du site Vitabulle est assurée par le programme 5D Multimédia & Communication de la structure Emploi Lausanne au sein de la Ville de Lausanne dont l'établissement est situé à Lausanne en Suisse.
</p>
<p>
Le responsable de la publication est Monsieur Olivier Dommange.
</p>

<h2>ARTICLE 3: Définitions</h2>
<p>
La présente clause a pour objet de définir les différents termes essentiels du contrat:
</p>
<ul>
    <li>Utilisateur: ce terme désigne toute personne qui utilise le site ou l'un des services proposés par l'application et le site Vitabulle.</li>
    
    <li>Contenu utilisateur: ce sont les données transmises par l'Utilisateur au sein de l'application ou du site Vitabulle.</li>

    <li>Membre: l'Utilisateur devient membre lorsqu'il est identifié sur l'application ou le site Vitabulle.</li>

    <li>Identifiant et mot de passe: c'est l'ensemble des informations nécessaires à l'identification d'un Utilisateur sur l'application ou le site Vitabulle. L'identifiant et le mot de passe permettent à l'Utilisateur d'accéder à des services réservés aux membres de l'application et du site Vitabulle. Le mot de passe est confidentiel.</li>
</ul>

<h2>ARTICLE 4: Accès aux services</h2>

<p>Le site permet à l'Utilisateur un accès gratuit aux services suivants:</p>

<ul>
    <li>Données personnelles librement inscrites par l'Utilisateur;</li>

    <li>Les statistiques et progression qu'il a notifiées depuis l'application;</li>

    <li>Des configurations et paramétrages des séances d'exercice.</li>
</ul>
<p>
L'application et le site Vitabulle est accessible gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les frais supportés par l'Utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.
</p>
<p>
Selon le cas:
</p>
<p>
Le site met en œuvre tous les moyens mis à sa disposition pour assurer un accès de qualité à ses services. L'obligation étant de moyens, le site ne s'engage pas à atteindre ce résultat.
</p>
<p>
Tout événement ayant pour conséquence un dysfonctionnement du réseau ou du serveur n'engage pas la responsabilité de l'application et du site Vitabulle.
</p>
<p>
L'accès aux services du site peut à tout moment faire l'objet d'une interruption, d'une suspension, d'une modification sans préavis pour une maintenance ou pour tout autre cas. L'Utilisateur s'oblige à ne réclamer aucune indemnisation suite à l'interruption, à la suspension ou à la modification du présent contrat.
</p>
<p>
Le service de l'application et du site Vitabulle sont gracieusement offerts. Aucun support à l'Utilisateur n'est fourni au-delà des informations qu'il retrouve dans l'application et le site Vitabulle.
</p>

<h2>ARTICLE 5: Propriété intellectuelle</h2>
<p>
Les marques, logos, signes et tout autre contenu de l'application et du site Vitabulle font l'objet d'une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d'auteur.
</p>
<p>
L'Utilisateur sollicite l'autorisation préalable de l'application et du site Vitabulle pour toute reproduction, publication, copie des différents contenus.
</p>
<p>
L'Utilisateur s'engage à une utilisation des contenus de l'application et du site Vitabulle dans un cadre strictement privé. Une utilisation des contenus à des fins commerciales est strictement interdite.
</p>
<p>
Tout contenu mis en ligne par l'Utilisateur est de sa seule responsabilité. L'Utilisateur s'engage à ne pas mettre en ligne de contenus pouvant porter atteinte aux intérêts de tierces personnes. Tout recours en justice engagé par un tiers lésé contre l'application ou du site Vitabulle sera pris en charge par l'Utilisateur.
</p>
<p>
Le contenu de l'Utilisateur peut être à tout moment et pour n'importe quelle raison supprimé ou modifié par l'application ou le site Vitabulle. L'Utilisateur ne reçoit aucune justification et notification préalablement à la suppression ou à la modification du contenu Utilisateur.
</p>

<h2>ARTICLE 6: Données personnelles</h2>
<p>
Les informations demandées lors de l’inscription sur le site ou sur l'application sont nécessaires et obligatoires pour la création du compte de l'Utilisateur. En particulier, l'adresse électronique pourra être utilisée par l'application et le site Vitabulle pour l'administration, la gestion et l'animation du service.
</p>
<p>
Le site et l'application assurent à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée.
</p>
<p>
L'Utilisateur dispose d'un droit d'accès, de rectification, de suppression de ses données personnelles. L'Utilisateur exerce ce droit via son espace personnel sur l'application et le site Vitabulle.
</p>

<h2>ARTICLE 7: Responsabilité et force majeure</h2>
<p>
Les sources des informations diffusées sur l'application et le site Vitabulle sont réputées fiables. Toutefois, l'application et le site Vitabulle se réserve la faculté d'une non-garantie de la fiabilité des sources. Les informations données sur l'application et le site Vitabulle le sont à titre purement informatif. Ainsi, l'Utilisateur assume seul l'entière responsabilité de l'utilisation des informations et contenus du présent site.
</p>
<p>
L'Utilisateur s'assure de garder son mot de passe secret. Toute divulgation du mot de passe, quelle que soit sa forme, est interdite.
</p>
<p>
L'Utilisateur assume les risques liés à l'utilisation de son identifiant et mot de passe. L'application et le site Vitabulle déclinent toute responsabilité.
</p>
<p>
Tout usage du service par l'Utilisateur ayant directement ou indirectement pour conséquence des dommages doit faire l'objet d'une indemnisation au profit de l'application et du site Vitabulle.
</p>
<p>
Une garantie optimale de la sécurité et de la confidentialité des données transmises n'est pas assurée par l'application ou le site Vitabulle. Toutefois, l'application et le site Vitabulle s'engage à mettre en œuvre tous les moyens nécessaires afin de garantir au mieux la sécurité et la confidentialité des données.
</p>
<p>
La responsabilité de l'application ou du site Vitabulle ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d'un tiers.
</p>

<h2>ARTICLE 8: Liens hypertextes</h2>
<p>
De nombreux liens hypertextes sortants sont présents sur l'application et le site Vitabulle, cependant les pages web où mènent ces liens n'engagent en rien la responsabilité de l'application et du site Vitabulle qui n'ont pas le contrôle du contenu de ces sources.
</p>
<p>
L'Utilisateur s'interdit donc à engager la responsabilité de l'application ou du site Vitabulle concernant le contenu et les ressources relatives à ces liens hypertextes sortants.
</p>
<h2>ARTICLE 9: Evolution du contrat</h2>
<p>
L'application et le site Vitabulle se réservent à tout moment le droit de modifier les clauses stipulées dans le présent contrat.
</p>
<h2>ARTICLE 10: Durée</h2>
<p>
La durée du présent contrat est indéterminée. Le contrat produit ses effets à l'égard de l'Utilisateur à compter de l'utilisation du service.
</p>
<h2>ARTICLE 11: Droit applicable et juridiction compétente</h2>
<p>
La législation suisse s'applique au présent contrat. En cas d'absence de résolution amiable d'un litige né entre les parties, seuls les tribunaux suisses sont compétents. Le for juridique est à Lausanne.
</p>

<h2>ARTICLE 12: Publication par l’Utilisateur</h2>
<p>
L'application et le site Vitabulle permet aux membres de partager des informations personnelles notamment sur les réseaux sociaux.
</p>
<p>
Dans ses publications, le membre s’engage à respecter les règles de la Netiquette et les règles de droit en vigueur.
</p>
<p>
L'application et le site Vitabulle n'exercent aucune modération sur les publications.
</p>
<p>
Le membre reste titulaire de l’intégralité de ses droits de propriété intellectuelle. Mais en partageant des informations depuis l'application ou le site VItabulle, il cède le droit non exclusif et gratuit de représenter, reproduire, adapter, modifier, diffuser et distribuer sa publication, directement ou par un tiers autorisé, dans le monde entier, sur tout support (numérique ou physique), pour la durée de la propriété intellectuelle.
</p>