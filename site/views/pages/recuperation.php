<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/chaussure.png">
        </div>
        <div class="col-lg-7 col-sm-9">



            <h1 class="page">Meilleure récupération après l’effort</h1>

                    <p>
                       La pratique de la Cohérence cardiaque permet de faire baisser le stress, de réguler le rythme cardiaque et donc permet aussi une meilleure récupération après l’effort. La pratique de la Cohérence cardiaque existe dans le milieu du sport de haut niveau depuis de nombreuses années. Ce fut l’une des toutes premières applications de la Cohérence cardiaque. Elle permet une meilleure récupération pendant et après l’effort pour les sportifs, même si l’on n’est pas un sportif de haut niveau.  
                    </p>
                    <p>
                        <a href="http://www.ifemdr.fr/" class="vert">Selon l’Institut français EMDR</a> (Eye Movement Desensitization &amp; Reprocessing), méthode de psychothérapie basée sur le mouvement des yeux, «<a href="http://www.ifemdr.fr/la-coherence-cardiaque-pour-les-sportifs/" class="vert">plusieurs sportifs</a> comme Tiger Wood, Michael Jordan ou Florent Manaudou, pratiquent la Cohérence cardiaque dans un but d’optimisation de leurs performances.»  Et le site de poursuivre: «La Cohérence cardiaque permet également de mieux gérer toutes les situations (gestion de l’échec, peur de gagner…) afin de prendre les bonnes décisions au milieu de l’action et d’assurer un geste fluide, relâché,  performant. La Cohérence cardiaque peut être utilisée avant, pendant et après les compétitions.»
                    </p>

            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>