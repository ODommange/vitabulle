<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/choc.png">
        </div>
        <div class="col-lg-7 col-sm-9">
           
            
            
            <h1 class="page">Meilleure tolérance à la douleur</h1>
            

            <p>
                Entre autres bienfaits, la méthode de la Cohérence cardiaque réduit les états de tension, ainsi qu’exposé sur la carte <a href="https://www.figma.com/file/PqUTV3Zmo5cEvZEw93h0dEk3/UX?node-id=79%3A37" class="vert">Diminution du stress.</a>
            </p>
            <p>
                Une pratique régulière – et ceci a été confirmé dans de nombreuses études scientifiques au cours de ces deux dernières décennies – témoigne également d’un champ d’action plus large et de bénéfices multiples quant au mieux-être. L’anxiété, l’angoisse, les troubles quotidiens, que ce soit au travail ou à la maison, génèrent des maux qui peuvent aller des douleurs musculaires, dorsales, digestives aux migraines, en passant par de multiples souffrances physiques de tous ordres.
            </p>
            <p>
                Par son impact positif sur une baisse de l’état de tension en général, la Cohérence cardiaque permet une meilleure tolérance de la douleur lors d’inflammation chronique, par exemple.
                Ainsi qu’on peut le lire sur le site <a href="https://www.medinat.fr/blog/189/comprendre-la-coherence-cardiaque" class="vert">Medinat</a>: «Le champ d’action de la Cohérence cardiaque est large. Elle peut être utilisée pour atténuer certaines douleurs physiques et soulager les troubles psychiques. Comme il s’agit d’une pratique relativement douce, tout le monde peut y recourir, quels que soient l’âge et la condition physique. Ainsi, même les individus qui souffrent de troubles cardiovasculaires, de maladie chronique en tout genre ou encore de diabète peuvent recourir à la cohérence cardiaque pour atténuer les symptômes de leurs maladies ou les douleurs qui accompagnent celles-ci».
            </p>
            
            
            
            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">Consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>