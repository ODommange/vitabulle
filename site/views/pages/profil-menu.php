<?php if( isset( $datas ) && is_array( $datas ) ) { ?>

<ul class="user_menu">
    <?php foreach( $datas as $menu ){ ?>
    <li><a class="<?php echo ( $menu[ 'selected' ] ) ? 'selected' : ''; ?>" href="<?php echo SITE_URL . $menu[ 'url' ]; ?>"><?php echo $menu[ 'name' ]; ?></a></li>
    <?php } ?>
    <li><a href="<?php echo SITE_URL; ?>/login/disconnect">Se déconnecter</a></li>
</ul>

<?php }