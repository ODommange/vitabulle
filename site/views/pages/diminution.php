<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/cardiogramme.png">
        </div>
        <div class="col-lg-7 col-sm-9">
            
            
            
            <h1 class="page">Diminution de l’hypertension artérielle</h1>
            
            <div class="lead">Dans son livre «Cohérence cardiaque 365», le Dr David O'Hare, détaille <a href="https://www.thierrysouccar.com/bien-etre/info/les-effets-de-la-coherence-cardiaque-989" class="vert">l'effet de cette technique</a> de respiration sur la défense immunitaire. </div>
            

                    <p>
                        "Diminution de  <a href="https://www.thierrysouccar.com/sante/info/quest-ce-que-lhypertension-arterielle-529" class="vert">l’hypertension artérielle</a> pour les hypertensions artérielles légères à modérées. Cette diminution est à présent reconnue comme étant un traitement intéressant avant la mise en place de médicaments". Extrait du livre "Cohérence cardiaque 365", 2012, paru aux Ed. Thierry Souccar.
                    </p>
                    <p>
                        A la tête de l'Académie  <a href="https://www.masantenaturelle.com/jean-pierre_demets.html" class="vert">Ma Santé Naturelle</a>, le naturopathe Jean-Pierre Demets fait le lien entre baisse de la tension et Cohérence cardiaque. Il explique: «Dans son livre «Guérir», le  <a href="https://www.coherenceinfo.com/videotheque/videotheque-david-servan-schreiber/" class="vert">Dr David Servan-Schreiber</a> décrit la technique de la Cohérence cardiaque telle que pratiquée par le Heartmath Institute. Cette technique est en fait une technique de relaxation, elle consiste à apaiser les battements cardiaques par l'utilisation dans un premier temps de respirations profondes, et dans un deuxième et troisième temps par concentration mentale sur le cœur. Il se produit alors un apaisement du rythme   cardiaque et de la pression artérielle. La relaxation psychosomatique permet d'obtenir plus facilement le même résultat. 

                        Il est vital de rechercher le calme et la détente. Rappelez-vous que le stress entraîne immanquablement une élévation de la pression artérielle.» 
                    </p>
                    <p>
                        Comme on le lit sur le site spécialisé en santé  <a href="https://www.onmeda.fr/" class="vert">Onmega.fr</a>: &laquo;La Cohérence cardiaque est un  <a href="https://www.onmeda.fr/forme-et-bien-etre/coherence-cardiaque.html" class="vert">outil thérapeutique.</a>  Après une séance de Cohérence cardiaque, la fréquence cardiaque et la pression artérielle baissent doucement… Cette thérapie par la respiration permet ainsi de protéger notre santé cardiovasculaire,  <a href="https://www.onmeda.fr/forme-et-bien-etre/coherence-cardiaque.html" class="vert">combattre l’hypertension</a> et les maladies générées par le stress chronique. En contrôlant notre respiration, nous pouvons contrôler notre rythme cardiaque mais aussi réguler nos émotions et ainsi apaiser notre cerveau! A l’instar du yoga, de la méditation ou de la relaxation, la Cohérence cardiaque permet de réguler le stress et l’anxiété. Et donc de réduire également les maladies sensibles au stress, comme les maladies auto-immunes et cardiovasculaires.  C’est un véritable outil thérapeutique».
                    </p>
                    <p>
                        Selon le mensuel <em>«Santé Magazine»</em>, 20 millions de Français souffriraient d'hypertension. 
                        Et de vanter la technique de la Cohérence cardiaque pour apprivoiser le stress, et faire donc  <a href="https://www.santemagazine.fr/medecines-alternatives/approches-naturelles/5-facons-naturelles-de-faire-baisser-son-hypertension-173304" class="vert">diminuer l'hypertension artérielle.</a> «Sans être la cause principale de l’hypertension artérielle,  <a href="https://www.santemagazine.fr/sante/fiche-maladie/stress-177599" class="vert">le stress</a> peut l’aggraver, surtout chez les personnes exposées au stress professionnel, à des accès de colère, ou encore ayant un tempérament intériorisé. Une série d’études a montré l’efficacité de la Cohérence cardiaque  à réduire l’hypertension artérielle liée au stress. 
                        La Cohérence cardiaque est une respiration cadencée: inspirez par le nez 5 secondes, marquez un temps d’arrêt, puis expirez par le nez ou la bouche sur 5 secondes en vous concentrant sur votre souffle. L’exercice doit durer 5 minutes, le temps nécessaire pour équilibrer le système nerveux qui régule notre pression artérielle et notre fréquence cardiaque. Dans l’idéal, pratiquez-le trois fois par jour.» Et le mensuel de conclure: «Souvent évoqués pour réduire le stress, le yoga, l’acupuncture et la relaxation n’ont pas démontré d’effets comparables.»

                    </p>

            
            
            
            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">Consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>