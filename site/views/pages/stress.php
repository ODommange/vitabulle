<section id="pages">
    <div class="row">
        <div class="col-lg-3 col-sm-9">
            <img class="icone" src="<?php echo SITE_URL; ?>/theme/images/arbre.png">
        </div>
        <div class="col-lg-7 col-sm-9">
            

            <h1 class="page">Diminution du stress avec une baisse du cortisol</h1>
            
            <div class="lead">Baisse du cortisol, la principale hormone de défense secrétée lors d’un état de tension</div>

            
            <p>
                Qu’est-ce que le cortisol? Le <a href="https://fr.wikipedia.org/wiki/Cortisol" class="vert">cortisol</a> est une hormone, parfois surnommée aussi l’hormone du stress.
                Ainsi que l’explique le magazine <a href="http://sante.lefigaro.fr/sante/analyse/cortisol/quest-ce-que-cest" class="vert">Figaro Santé</a>: «Le cortisol est une hormone fabriquée par les glandes surrénales. Cette hormone joue un rôle essentiel dans l’équilibre du glucose sanguin et la libération de sucre à partir des réserves de l’organisme en réponse à une demande accentuée en énergie. Le cortisol intervient aussi dans le métabolisme des graisses et des protéines. Il joue un rôle d’anti-inflammatoire et participe à la régulation du sommeil.»
            </p>
            <p>
                Plus spécifiquement encore, on notera à propos de la <a href="https://fr.wikipedia.org/wiki/Cortisol" class="vert">production du cortisol</a> : «son excrétion est régulée au niveau de l'hypothalamus qui stimule l'hypophyse par la Corticotropin-releasing hormone (CRH). L'hypophyse sécrète alors l'hormone corticotrope ou ACTH qui stimule la glande surrénale. Le cortisol est alors excrété.»
            </p>
            <p>
                Spécialiste de la première heure, le Dr David O'Hare expose dans son  <a href="https://www.thierrysouccar.com/bien-etre/livre/coherence-cardiaque-365-107" class="vert">livre</a> comment le cortisol est la principale hormone de défense sécrétée pendant un stress. "C'est l'action de la <a href="https://www.thierrysouccar.com/bien-etre/info/les-effets-de-la-coherence-cardiaque-989" class="vert">Cohérence cardiaque sur le cortisol</a> qui explique une grande partie des effets de cette pratique sur le stress.
                Et l’auteur d’évoquer ici l’«augmentation de la DHEA (déhydroépioandrostérone), une hormone qui entre en jeu pour moduler le cortisol». Extrait du livre «Cohérence cardiaque 365», 2012, paru aux Ed. Thierry Souccar.
            </p>
            <p>
                Pour en savoir davantage sur le cortisol et les effets de la cohérence cardiaque sur le stress, vous pouvez aller vous promener sur ces différents sites, dont voici quelques liens: <a href="https://www.coherenceinfo.com/" class="vert">coherenceinfo</a>, fiche santé sur le cortisol,  <a href="https://www.prevention-sante.eu/psychologie/comprendre-mecanismes-stress-effets-deleteres-sante-1" class="vert">les mécanismes du stress</a>, <a href="http://inspirezvous.over-blog.com/article-hormones-du-stress-112151908.html" class="vert">les hormones du stress</a>, <a href="https://www.passeportsante.net/fr/Maux/analyses-medicales/Fiche.aspx?doc=analyse-cortisol-sang" class="vert"> le rôle du cortisol dans le sang</a>, <a href="https://www.neurosciences.asso.fr/V2/GrdPublic/pdf/FichesCerveau-Chapitre07.pdf" class="vert"> stress aigu et stress chronique</a>, etc.
            </p>
            <p>
                Le site français coherence-cardiaque.com explique comment <a href="http://www.coherence-cardiaque.com/approfondir_gestion_stress.html" class="vert">aa Cohérence cardiaque fait diminuer rapidement les symptômes du stress.</a>
            </p>
            <p>
                Selon le site suisse <a href="https://vitagate.ch/public/wem_files/Franzoesisch/Stress/hormones_stress.pdf" class="vert">Vitagate.ch</a> «Le cortisol est une hormone qui, en cas de stress, met de l’énergie (par exemple sous forme de sucre) à disposition du corps. La concentration de cortisol dans le sang varie considérablement au cours de la journée. Ce phénomène peut contribuer à protéger le corps contre des graves inflammations. Mais cela rend aussi le corps plus sensible à d’autres problèmes, notamment les infections.»
            </p>
            
            
            <div class="col-lg-2 col-sm-9">
                <div class="bouton-page center">
                    <div class="plus"> <a href="<?php echo SITE_URL; ?>/home#cards">Consulter les autres bienfaits ></a></div>
                </div>
            </div>
        </div>
    </div>
</section>