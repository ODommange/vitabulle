/**
 * Set user's points depending on it's behaviour. Are set in :
 *   - 2 points per exercices
 *   - 2 points for each exercices who has 3 hours in between
 *   - Points from his evaluation (from 1 to 3)
 *   - No more than 3 exercices per day are counted
 *   - How much days until user's have 60 points.
 * 
 * A day can have 21 points.
 * Are considered the last 7 days records.
 * 
 * No 1 Ballon : 60 points in 3 days
 * No 2 Ballon : 50 points in 3 days
 * No 3 Ballon : 40 points in 3 days
 * No 4 Ballon : 90 points overall days   OR   Today has 3 pratices   OR   Last 3 days has at leat 1 pratice every days
 * No 5 Ballon : 70 points overall days   OR   Today has 2 pratices   OR   Last 2 days has at leat 1 pratice every days
 * No 6 Ballon : 50 points overall days   OR   Today has 1 pratice
 * No 7 Ballon : Beginning
 * 
 */

var displayBasicHome = function()
{
    if( getSession('img') )
    {
        document.querySelector( '#bubulle img' ).src     = getSession('img');
        document.querySelector( '#bubulle p' ).innerHTML = getSession('txt');
    }
    else
    {
        document.querySelector( '#bubulle img' ).src     = content.bulle7.img;
        document.querySelector( '#bubulle p' ).innerHTML = content.bulle7.txt;  
    }

     home(); // Swiper function
};



    if( isLoguedIn() )
    {
        var content = {
                'bulle1':{
                    "img":"public/medias/game/game_07.png",
                    "txt":"Bravo! <br>Ta pratique est constante et te permet de profiter pleinement des effets positifs de la Cohérence cardiaque."
                },
                'bulle2':{
                    "img":"public/medias/game/game_06.png",
                    "txt":"C'est très bien! <br>Ta pratique est constante."
                },
                'bulle3':{
                    "img":"public/medias/game/game_05.png",
                    "txt":"Splendide! <br>Tu as atteint un objectif très intéressant."
                },
                'bulle4':{
                    "img":"public/medias/game/game_04.png",
                    "txt":"C'est bien! <br>Poursuis tes efforts et sois régulier."
                },
                'bulle5':{
                    "img":"public/medias/game/game_03.png",
                    "txt":"Tu es sur la bonne voie! <br>Continue et reste constant!"
                },
                'bulle6':{
                    "img":'public/medias/game/game_02.png', 
                    "txt":"Vitabulle a besoin d'un petit boost! <br>Réalise 3 séances aujourd'hui, ça ira déjà beaucoup mieux, pour toi aussi..."
                },
                'bulle7':{
                    "img":'public/medias/game/game_01.png', 
                    "txt":"Vitabulle a besoin de toi! <br>Démarre une séance, ça ira de mieux en mieux, pour toi aussi..."
                }
            };
        var nameMonth   = [ 'Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre' ]
        var cDate       = new Date();
        var cDay        = ( parseInt( cDate.getDate() ) < 10 ) ? '0' + cDate.getDate() : cDate.getDate();
        var to          = cDate.getFullYear() + '-' + ( cDate.getMonth() + 1 ) + '-' + cDay;
        
        document.querySelector('#today h1').innerHTML = cDate.getDate() + ' ' + nameMonth[ cDate.getMonth() ] + ' ' +  cDate.getFullYear();

        var dateOffset  = (24*60*60*1000) * 7; //7 days
        var cTimeOffset = cDate.getTime() - dateOffset;
        var cDateOffset = new Date( cTimeOffset );
        var cDayOffset  = ( parseInt( cDateOffset.getDate() ) < 10 ) ? '0' + cDateOffset.getDate() : cDateOffset.getDate();
        var from        = cDateOffset.getFullYear() + '-' + ( cDateOffset.getMonth() + 1 ) + '-' + cDayOffset;
        
        var eval = 0;
        var regularity = [];
        var nDay = 0;
        var todayNbPratices = 0;
        var threeLastDaysPractices = true;
        var twoLastDaysPractices = true;

        var request = new XMLHttpRequest();

        request.open( 'GET', url + 'api/users/stats/' + getSession('Token') + '/' + getSession('Id') + '/fromto/' + from + '/' + to + '/days', true );

        request.onload = function() 
        {
            if( request.status >= 200 && request.status < 400 )
            {
                var datas = JSON.parse( request.responseText );

                if( Array.isArray( datas ) )
                {
                    var nbDays = datas.length; 

                    for( var d = ( nbDays ); d >= 0; d-- )
                    {                    
                        if( datas[ d ] && datas[ d ].length > 0 )
                        {                        
                            var nbPractices = datas[ d ].length;

                            eval += nbPractices * 2;  // each practice +2
                            var practices = [];

                            for( var p = 0; p < nbPractices; p++ )
                            {
                                if( p < 3 ) // Max 3 exercices a day are counted
                                {
                                    practices[ p ] = datas[ d ][ p ].hour;

                                    eval += parseInt( datas[ d ][ p ].eval ); // add each evaluation points

                                    eval += ( p === 0 || ( p > 0 && ( parseInt( practices[ ( p ) ] ) - parseInt( practices[ p - 1 ] ) ) > 3 ) ) ? 2 : 0; // if 3 hours in between +2
                                }
                            }   
                            regularity[ nDay ] = eval;                     
                        }
                        else
                        {
                            if( nDay < 3 )
                            {
                                threeLastDaysPractices = false;
                            }
                            if( nDay < 2 )
                            {
                                twoLastDaysPractices = false;
                            }
                            regularity[ nDay ] = false;
                        }
                        nDay++;
                    }

                    todayNbPratices = datas[ ( nbDays - 1 ) ].length;

                    var todayTxt = 'Aucune séance n\'a encore eu lieu aujourd\'hui.';

                    if( todayNbPratices > 0 )
                    {
                        todayTxt = '<table>';

                        for( var t = 0; t < todayNbPratices; t++ )
                        {
                            if( t <= 2 )
                            {
                                todayTxt += '<tr><td>'+datas[ ( nbDays - 1 ) ][ t ].hour+'h'+datas[ ( nbDays - 1 ) ][ t ].min+'</td><td><img src="public/medias/eval_0'+datas[ ( nbDays - 1 ) ][ t ].eval+'.png"></td></tr>';
                            }
                        }

                        if( todayNbPratices > 2 )
                        {
                            todayTxt += '<tr><td>...</td><td></td></tr>';
                        }

                        todayTxt += '</table>';
                    }

                    if( todayNbPratices > 3 )
                    {
                        todayTxt += '<p class="small"><em>Mise en garde : </em>Comme pour toutes bonnes choses, il ne faut pas abuser. Les médecins recommandent une pratique quotidienne de la cohérence cardiaque trois fois par jour. Il est vivement recommandé de ne pas aller au-delà de quatre fois par jour.</p>';
                    }
                    
                    if( document.querySelector('#today div') )
                    {
                        document.querySelector('#today div').innerHTML = todayTxt;
                    }
                    
                    if( document.querySelector( '#bubulle img' ) )
                    {
                        if( regularity[ 2 ] >= 60 )
                        {
                            document.querySelector( '#bubulle img' ).src     = content.bulle1.img;
                            document.querySelector( '#bubulle p' ).innerHTML = content.bulle1.txt;
                            setSession('img', content.bulle1.img);
                            setSession('txt', content.bulle1.txt);
                        }
                        else if( regularity[ 2 ] >= 50 )
                        {
                            document.querySelector( '#bubulle img' ).src     = content.bulle2.img;
                            document.querySelector( '#bubulle p' ).innerHTML = content.bulle2.txt;
                            setSession('img', content.bulle2.img);
                            setSession('txt', content.bulle2.txt);
                        }
                        else if( regularity[ 2 ] >= 40 )
                        {
                            document.querySelector( '#bubulle img' ).src     = content.bulle3.img;
                            document.querySelector( '#bubulle p' ).innerHTML = content.bulle3.txt;
                            setSession('img', content.bulle3.img);
                            setSession('txt', content.bulle3.txt);
                        }
                        else if( eval >= 90 || todayNbPratices >= 3 )
                        {
                            document.querySelector( '#bubulle img' ).src     = content.bulle4.img;
                            document.querySelector( '#bubulle p' ).innerHTML = content.bulle4.txt;
                            setSession('img', content.bulle4.img);
                            setSession('txt', content.bulle4.txt);
                        }
                        else if( eval >= 70 || todayNbPratices === 2 || threeLastDaysPractices )
                        {
                            document.querySelector( '#bubulle img' ).src     = content.bulle5.img;
                            document.querySelector( '#bubulle p' ).innerHTML = content.bulle5.txt;
                            setSession('img', content.bulle5.img);
                            setSession('txt', content.bulle5.txt);
                        }
                        else if( eval >= 50 || todayNbPratices === 1 || twoLastDaysPractices )
                        {
                            document.querySelector( '#bubulle img' ).src     = content.bulle6.img;
                            document.querySelector( '#bubulle p' ).innerHTML = content.bulle6.txt;
                            setSession('img', content.bulle6.img);
                            setSession('txt', content.bulle6.txt);
                        }
                        else
                        {
                            document.querySelector( '#bubulle img' ).src     = content.bulle7.img;
                            document.querySelector( '#bubulle p' ).innerHTML = content.bulle7.txt;
                            setSession('img', content.bulle7.img);
                            setSession('txt', content.bulle7.txt);
                        }
                    }
                }
                else
                {
                    if( getSession('img') )
                    {
                        document.querySelector( '#bubulle img' ).src     = getSession('img');
                        document.querySelector( '#bubulle p' ).innerHTML = getSession('txt');
                    }
                    else
                    {
                        document.querySelector( '#bubulle img' ).src     = content.bulle7.img;
                        document.querySelector( '#bubulle p' ).innerHTML = content.bulle7.txt;  
                    }
                }

                home(); // Swiper function
            }
            else
            {
                displayBasicHome();
            }
        };
    
        request.onerror = function()
        {
            displayBasicHome();
        };
        
        request.send();
    }

