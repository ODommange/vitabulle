var calendar = function()
{
    var currentDate = new Date(datas.Y, ( datas.m - 1 ), 1); // Premier date
    var todayDate   = new Date(); // Today
    
    var oMonths = {
        name:[ 'Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre' ],
        days:[ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ]
    };
    var curDay      = currentDate.getDate();
    var kMonth      = currentDate.getMonth();
    var curYear     = currentDate.getFullYear();

    var todayDay    = todayDate.getDate();
    var kMonthDay   = todayDate.getMonth();
    var todayYear   = todayDate.getFullYear();

    curDay = ( kMonth !== kMonthDay || curYear !== todayYear ) ? 0 : todayDay;
   
    if( curYear % 4 === 0 && curYear !== 1900 )
    {
        oMonths.days[ 1 ] = 29;
    }
    
    document.querySelector( 'tr#cal_month th h4' ).innerHTML = oMonths.name[ kMonth ] + ' ' + curYear;

    currentDate.setDate( 1 ); // Firstday of the month
    var dayOne  = currentDate.getDay();
    var week    = 1;
    var weekDay = 1;
    var nbDays  = oMonths.days[ kMonth ]; 

    for( i = 1; i < dayOne; i++ )
    {
        var dayTdCell = document.querySelector( 'div#calendar tr:nth-child( ' + ( week ) + ' ) td:nth-child( ' + ( weekDay ) + ' )' );
        dayTdCell.classList.add( 'cal_prev_days' );
        dayTdCell.innerHTML = oMonths.days[ kMonth - 1 ] - dayOne + i;
        weekDay++;
    }

    for( i = 1; i <= nbDays; i++ )
    {
        var nbPractices         = datas.apiDatas[ ( i - 1 ) ].values.length;
        var displayPractices    = ( nbPractices > 0 ) ? '<span>' + nbPractices + '</span>' : '<span></span>';
        var dateUrl             = '/Y=' + curYear + '/m=' + (kMonth + 1) + '/d=' + i;
        var classDate           = ( curYear <= todayYear && kMonth <= kMonthDay && ( ( i <= todayDay && curDay !== 0 ) || ( curDay === 0 )) ) ? 'cal_pass' : 'cal_future'
                
        if( curDay === i )
        {
            var dayTdTodayCell = document.querySelector( 'div#calendar tr:nth-child('+ ( week ) +') td:nth-child( ' + ( weekDay ) + ' )' );
            dayTdTodayCell.classList.add( 'cal_today' );
            dayTdTodayCell.setAttribute( 'data-date', dateUrl );
            dayTdTodayCell.innerHTML =  i + displayPractices;
        }
        else
        {
            var dayTdOtherCell = document.querySelector( 'div#calendar tr:nth-child('+ ( week ) +') td:nth-child( ' + ( weekDay ) + ' )' );
            dayTdOtherCell.classList.add( classDate );
            dayTdOtherCell.setAttribute( 'data-date', dateUrl );
            dayTdOtherCell.innerHTML = i + displayPractices;
        }
        weekDay++;
        if( weekDay === 8 )
        {
            weekDay = 1;
            week++;
        }
    }
    for( i = 1; weekDay !== 1; i++ )
    {
        var dayTdNextCell = document.querySelector( 'div#calendar tr:nth-child( ' + ( week ) + ' ) td:nth-child( ' + ( weekDay ) + ' )' );
        dayTdNextCell.classList.add('cal_next_days');
        dayTdNextCell.innerHTML = i;
        weekDay++;
        if( weekDay === 8 )
        {
            weekDay = 1;
            week++;
        }
    }
    if( week <= 6 )
    {
        for( j = 1; weekDay <= 7; j++ )
        {
            var dayTdLastCell = document.querySelector( 'div#calendar tr:nth-child( ' + ( week ) + ' ) td:nth-child( ' + j + ' )' );
            dayTdLastCell.classList.add('cal_next_days');
            dayTdLastCell.innerHTLML = i++;
            weekDay++;
        }
    }

    var calendarDates = document.querySelectorAll( 'div#calendar td[data-date].cal_pass, div#calendar td[data-date].cal_today' );
    
    Array.prototype.forEach.call( calendarDates, function( calendarDate )
    {
        calendarDate.onclick = function()
        {
           location.hash = 'results-day' + this.dataset.date;
        };
    });
    
    var wHeight = window.innerHeight;
    
    var expand = document.querySelector( 'div.calendar-bkg' ).getBoundingClientRect().top + document.body.scrollTop;
        
    var calendarCells = document.querySelectorAll( 'div#calendar td' );
    
    Array.prototype.forEach.call( calendarCells, function( calendarCell )
    {
        calendarCell.style.height = Math.floor(( wHeight - expand - 10 ) / 6 ) + 'px';
    });
    
};