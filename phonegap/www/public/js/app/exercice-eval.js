    var evalChoices = document.querySelectorAll( 'figure.eval div' );

    if( evalChoices && evalChoices.length > 0 )
    {
        Array.prototype.forEach.call( evalChoices, function( evalChoices )
        {
            evalChoices.onclick = function()
            {
                var timestamp   = Math.round(+new Date()/1000);
                var eval        = this.dataset.eval;
                var id          = getSession('Id');

                var request = new XMLHttpRequest();

                request.open( 'GET', url + 'api/users/stats/' + getSession( 'Token' ) + '/' + id + '/insert/' + eval + '/' + timestamp, true );

                request.onload = function() 
                {
                    if( request.status >= 200 && request.status < 400 )
                    {
                        var datas = JSON.parse( request.responseText );

                        if( datas.process === 'success' )
                        {
                            location.hash = 'exercice-result/eval=' + eval;
                        }
                        else
                        {
                            location.hash = 'exercice-result';
                        }
                    }
                };

                request.onerror = function()
                {
                    location.hash = 'exercice-result';
                };

                request.send();
            };
        });
    }
