var home = function()
{
    if( document.querySelector( '.swiper-home-container' ) )
    {
        var swiperHome = new Swiper('.swiper-home-container', {
           loop : true,
           stopOnLastSlide : false,
           pagination: {
             el: '.swiper-home-pagination'
           },
           breakpoints: {
             1920: {
               slidesPerView: 1,
               spaceBetween: 0
             }
           },
           autoplay: false
        });
    }
};


var about = function()
{ 
    var swiperAbout = new Swiper('.swiper-container', {
        loop : false,
        stopOnLastSlide : true,
        pagination: {
          el: '.swiper-pagination'
        },
        breakpoints: {
              1920: {
                slidesPerView: 1,
                spaceBetween: 0
              }
        },
        autoplay: false,
        preloadImages : true, 
        updateOnImagesReady : true
    });
};