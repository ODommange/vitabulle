$( function() {
   
    var particuleAnim   = null;
    var exerciceAnim    = null;
    
    var delayStratAnim  = null;
    var animAttente     = null;
    var animInspire     = null;
    var animExpire      = null;
    
    var AmbienceSoundFile = '';
    var BreathSoundFile   = '';
    
    clearInterval( exerciceAnim );
    clearInterval( particuleAnim );
    clearTimeout( animAttente );
    clearTimeout( animInspire );
    clearTimeout( animExpire );
    
    var soufflesParMin      = 6;
    var dureeTotalExercice  = 5; // minutes (1 = 60 seconds)

    var particulesSize      = 14;
    var dureeInspiration    = 5000 * 6 / soufflesParMin;
    var dureeExpiration     = 5000 * 6 / soufflesParMin;
    var dureeAttente        = 0 * 6 / soufflesParMin;;
    var easeInspiration     = 'easeInOutCubic';
    var easeExpiration      = 'easeInOutCubic'; // 'easeInOutQuart'
    var duree               = 0;
    var countdown           = 2;

    var particuleIsSet  = false;
    var animIsOn        = true;
    var particules      = null;
    var animdist        = 0;
    var particulesPosition = [];
    var particulemeterSize = 0;

    var compressAmplitude   = 1;
    var expendAmplitude     = 1;

    var soundTrack          = 0;
    var soundVolume         = 1;
    var intervalSoundFadeOut= null;
    var isFadingOut         = false;
    var isFadingOutDuration = 0.2; // minutes (1 = 60 seconds)

    var isSoundOn           = true;
    var isCountdownOn       = true;
    var isTextSupportOn     = true;
    
    
    var hashPage = 'exercice-eval';
    
    $('header > a').on('click', function( e )
    {
        e.preventDefault();
        hashPage = 'home';
        duree = dureeTotalExercice;
    });

    var textSupport = function( breath )
    {
        if( isTextSupportOn )
        {
            $('div.textsupport').html( breath ).addClass('appear');

            setTimeout(function(){
                $('div.textsupport').removeClass('appear');
            }, ( dureeInspiration / 6 * soufflesParMin - 2500 ) );
        }  
    };

    var expireMeterOut = function()
    {
        $('div.particulemeterSmall').animate({opacity:0}, 1750);
    };

    var expireMeter = function()
    {        
        $('div.particulemeterSmall').stop(true, true).animate({opacity:1}, 1750, function(){ expireMeterOut(); });
    };


    var inspireMeterOut = function()
    {
        $('div.particulemeterBig').animate({opacity:0}, 1750);
    };

    var inspireMeter = function()
    {     
        clearInterval( delayStratAnim );
        $('div.particulemeterBig').stop(true, true).animate({opacity:1}, 1750, function(){ inspireMeterOut(); });   
    };

    /**
     * Breath animation
     * n1 is Inspiration
     * n2 is Expiration
     * n3 is Waiting
     * @returns void
     */  
    var animcycle = function()
    {
        var n1 = 0;
        var n2 = 0;
        var n3 = 0;

        $.each( particulesPosition, function( k, v)
        {    
            var o = particulesPosition[k].obj;
            o.stop(true, true);

            n1++;
            var l = particulesPosition[k].posLeft;
            var t = particulesPosition[k].posTop;
            var a = particulesPosition[k].animdist;

            if( n1 === 1 )
            {   
                if( isSoundOn && document.querySelector('audio#audio_loop1') && document.querySelector('audio#audio_loop2') )
                {
                    var track = ( soundTrack % 2 === 0 ) ? 'audio_loop2' : 'audio_loop1';
                    soundTrack++;
                    document.querySelector('audio#'+track).currentTime = 0;        // AUDIO reset & PLAY
                    document.querySelector('audio#'+track).play();
                }

                textSupport( 'Inspire' );
                
                animExpire = setTimeout( function(){ inspireMeter(); }, ( dureeExpiration * 0.5 ) + dureeAttente ); 
            }

            o.animate({left:( ( l * animdist ) * a / 60 ), top:( ( t * animdist ) * a / 60 )}, dureeInspiration, easeInspiration, function()
            {
                n2++;
                if( n2 === 1 )
                {   
                    textSupport( 'Expire' );
                    
                    animInspire = setTimeout( function(){ expireMeter(); }, ( dureeInspiration * 0.5 ) + dureeAttente );
                }

                o.animate({left:( l ), top:( t )}, dureeExpiration, easeExpiration, function()
                {
                    n3++;
                    if( n3 === 1 && animIsOn )
                    { 
                        animAttente = setTimeout( function(){ animcycle(); }, ( dureeAttente * 0.5 ) + dureeAttente );
                    }
                });
            });
        });
    };


    /**
     * It fades the sounds
     * Used at the end of the breath animation
     * 
     * @returns void
     */
    var soundFadeOut = function()
    {
        soundVolume -= 0.01;
        if( soundVolume >= 0 )
        {
            if( document.querySelector('audio#audio_bkg') )
            {
                document.querySelector('audio#audio_bkg').volume = soundVolume;
            }
            if( document.querySelector('audio#audio_loop1') )
            {
                document.querySelector('audio#audio_loop1').volume = soundVolume;
            }
            if( document.querySelector('audio#audio_loop2') )
            {
                document.querySelector('audio#audio_loop2').volume = soundVolume;
            }
        }
        else
        {
            clearInterval( intervalSoundFadeOut );
            if( document.querySelector('audio#audio_bkg') )
            {
                document.querySelector('audio#audio_bkg').volume = 0;
            }
            if( document.querySelector('audio#audio_loop1') )
            {
                document.querySelector('audio#audio_loop1').volume = 0;
            }
            if( document.querySelector('audio#audio_loop2') )
            {
                document.querySelector('audio#audio_loop2').volume = 0;
            }
        }
    };

    /**
     * The exercice manager. 
     * Starts Counter (background animation one), Sound and Breath animation. 
     * Ends the process when counting is done.
     * 
     * @returns void
     */
    var doExercice = function()
    {
        duree += 1/60;

        if( duree < dureeTotalExercice )
        {
            if( isSoundOn && dureeTotalExercice - duree < isFadingOutDuration && !isFadingOut )
            { 
                isFadingOut = true;
                intervalSoundFadeOut = setInterval( function(){ soundFadeOut(); }, ( isFadingOutDuration * 600 ) );
            }
            setTimeout( function(){ doExercice(); }, 500 );
        }
        else
        {
            $('div.duree').animate({opacity:0}, 500);
            $('div.textsupport').animate({opacity:0}, 500);
            $('div.particules > span').fadeOut( 1000, function()
            {
                $('div.particulemeter').fadeOut( 1000, function()
                {
                    animIsOn = false;
                    location.hash = hashPage;
                });
            });
        } 
    };
    
    /**
     * Set and play sounds
     * @param {string} AmbienceSoundFile  Filename of sound
     * @param {string} BreathSoundFile  Filename of sound
     * @returns {void}
     */
    var setAudio = function( AmbienceSoundFile, BreathSoundFile )
    {
        if( BreathSoundFile.length > 0 && AmbienceSoundFile.length > 0 )
        {
            document.querySelector( 'audio#audio_loop1 source' ).src = 'public/medias/audio/' + BreathSoundFile + '_1.mp3';
            document.querySelector( 'audio#audio_loop2 source' ).src = 'public/medias/audio/' + BreathSoundFile + '_2.mp3';
            document.querySelector( 'audio#audio_bkg source' ).src = 'public/medias/audio/' + AmbienceSoundFile + '.mp3';
            document.querySelector( 'audio#audio_loop1' ).load();
            document.querySelector( 'audio#audio_loop2' ).load();
            document.querySelector( 'audio#audio_bkg' ).load();
            document.querySelector( 'audio#audio_bkg' ).play(); // AUDIO loop1 PLAYS
        }
    };
    
    /**
     * The countdown before the breath animation begins
     * 
     * @returns void
     */
    var doCountdown = function()
    {
        if( countdown >= 1 )
        {
            $('div.countdown > span').animate({opacity:1}, 100).html(countdown);
        }
        countdown--;

        if( countdown >= 0 )
        {
            setTimeout( function(){ 

                if( countdown >= 1 )
                {
                    $('div.countdown > span').animate({opacity:0.3}, 100, function(){ doCountdown(); }); 
                }
                else
                {
                    $('div.countdown > span').animate({opacity:0}, 100, function(){ doCountdown(); }); 
                }
            }, 1000); 
        }
        else
        {
            $('div.countdown').fadeOut();
           
            if( isCountdownOn )
            {
                $('div.duree').animate({transform:'translate(100%,0)'}, ( dureeTotalExercice * 60000 ), 'linear' );
            }
            doExercice();
            
            animcycle();
        } 
    };


    /**
     * Sets all parameters (size, distance, start and end status) for the particules on the loading page
     * 
     * @returns void
     */
    var animateParticules = function()
    {
        animIsOn = true;

        duree = 0;
        particules              = null;
        intervalSoundFadeOut    = null;
        $('div.particulemeterSmall').stop();
        $('div.particulemeterBig').stop();
        $('div.particules span').remove();
        $('div.particulemeter.particulemeterSmall').remove();
        $('div.particulemeter.particulemeterBig').removeClass('particulemeterBig');
        
        isCountdownOn   = ( getSession( 'countdown' ) && ( getSession( 'countdown' ) === 'true' || getSession( 'countdown' ) === true ) ) ? true : false;
        isSoundOn       = ( getSession( 'sound' ) && ( getSession( 'sound' ) === 'true' || getSession( 'sound' ) === true ) ) ? true : false;
        isTextSupportOn = ( getSession( 'textsupport' ) && ( getSession( 'textsupport' ) === 'true' || getSession( 'textsupport' ) === true ) ) ? true : false;
        
        var amplitudeMax = 200; // Distance limite entre les points (inspire à expire)

        var wWidth  = $(window).width();
        var wHeight = $(window).height();

        var animdistMax = ( wWidth < wHeight )                  ? wWidth        : wHeight;
        animdist        = ( animdistMax * 0.25 > amplitudeMax ) ? amplitudeMax  : animdistMax * 0.25;

        compressAmplitude  = animdist / amplitudeMax;
        expendAmplitude    = amplitudeMax / animdist;

        particulemeterSize = animdist * 4 * 0.9 ; // 90% de la fenetre

        if( !particuleIsSet )
        {   
            $.each( particulesPositions, function( k, v)
            {
                $('div.particules').append('<span data-posleft="' + v[0] + '" data-postop="' + v[1] + '"></span>');
            });

            particules = $('div.particules > span');

            particules.css({
                width:( particulesSize * compressAmplitude * 1.45 ),
                height:( particulesSize * compressAmplitude * 1.45 ),  
                marginLeft : -( particulesSize * compressAmplitude * 0.725  ), 
                marginTop:-( particulesSize * compressAmplitude * 0.725 )
            });

            $('div.particulemeter').css({width:( 200 * compressAmplitude + 20 ), height:( 200 * compressAmplitude + 20 ), borderWidth:3, opacity:0})
                                   .addClass('particulemeterSmall');
            $('div.particulemeter').clone()
                                   .appendTo( 'main' )
                                   .css({width:particulemeterSize, height:particulemeterSize, borderWidth:5})
                                   .removeClass('particulemeterSmall')
                                   .addClass('particulemeterBig');
        }  
        else
        {
            $('div.particulemeter.particulemeterBig').css({width:particulemeterSize, height:particulemeterSize});
            $('div.particulemeter.particulemeterSmall').css({width:( 200 * compressAmplitude + 30 ), height:( 200 * compressAmplitude + 30 )});
        }          

        $.each( particules, function( k, v )
        {  
            var random  = Math.floor((Math.random() * 2));
            var random2  = Math.floor((Math.random() * 3));
            var posLeft = parseInt( $(this).data('posleft') );
            var posTop  = parseInt( $(this).data('postop') );

            var posX = ( Math.abs( posTop ) +  Math.abs( posLeft ) );

            var animdistFactor = 1.0 * expendAmplitude;

            posLeft *= compressAmplitude;
            posTop  *= compressAmplitude; 

            if( k % random !== 0 )
            {
                if( 20 > posX ){ animdistFactor = 0.5 * expendAmplitude; }
                else if( 40 > posX ){ animdistFactor = 0.6 * expendAmplitude; }
                else if( 60 > posX ){ animdistFactor = 0.7 * expendAmplitude; }
                else if( 80 > posX ){ animdistFactor = 0.8 * expendAmplitude; }
                else if( 100 >= posX ){ animdistFactor = 0.9 * expendAmplitude; }
            }

            particulesPosition[k] = {obj:$(this), posLeft:posLeft, posTop:posTop, animdist:animdistFactor, animate:random2};

            $(this).css({left:particulesPosition[k].posLeft, top:particulesPosition[k].posTop});
        });

        if( !particuleIsSet )
        { 
             if( isSoundOn )
            {
                if( getSession( 'ambience' ) && getSession( 'ambience' ).length > 0 && getSession( 'breath' ) && getSession( 'breath' ).length > 0 )
                {
                    AmbienceSoundFile = getSession( 'ambience' );
                    BreathSoundFile   = getSession( 'breath' );
                    
                    setAudio( AmbienceSoundFile, BreathSoundFile );
                }
                else
                {
                    var request = new XMLHttpRequest();

                    request.open( 'GET', 'public/json/sounds.json', true );

                    request.onload = function() 
                    {
                        if( request.status >= 200 && request.status < 400 )
                        {
                            var jsonAudios = JSON.parse( request.responseText );
                            
                            AmbienceSoundFile = jsonAudios.ambience[0][0];
                            BreathSoundFile   = jsonAudios.breath[0][0];
                    
                            setAudio( AmbienceSoundFile, BreathSoundFile );
                        }
                    };
                    
                    request.send();
                }
            }
            
            doCountdown();
        }

        particuleIsSet = true;
    }; 

    animateParticules();

    $(window).on('resize', function()
    {   
        animateParticules();
    });

});