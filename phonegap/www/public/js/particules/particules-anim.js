var particuleAnim   = null;
var exerciceAnim    = null;

var delayStratAnim  = null;
var animAttente     = null;
var animInspire     = null;
var animExpire      = null;

var AmbienceSoundFile = '';
var BreathSoundFile   = '';

clearInterval( exerciceAnim );
clearInterval( particuleAnim );
clearTimeout( animAttente );
clearTimeout( animInspire );
clearTimeout( animExpire );

var soufflesParMin      = 6;
var dureeTotalExercice  = 5; // minutes (1 = 60 seconds)

var particulesSize      = 14;
var dureeInspiration    = 5000 * 6 / soufflesParMin;
var dureeExpiration     = 5000 * 6 / soufflesParMin;
var dureeAttente        = 0 * 6 / soufflesParMin;;
var duree               = 0;
var countdown           = 2;

var particuleIsSet  = false;
var animIsOn        = true;
var particules      = null;
var animdist        = 0;
var particulesPosition = [];
var particulemeterSize = 0;

var compressAmplitude   = 1;
var expendAmplitude     = 1;

var soundTrack          = 0;
var soundVolume         = 1;
var intervalSoundFadeOut= null;
var isFadingOut         = false;
var isFadingOutDuration = 0.2; // minutes (1 = 60 seconds)

var isSoundOn           = true;
var isCountdownOn       = true;
var isTextSupportOn     = true;


var hashPage = 'exercice-eval';

if( document.querySelector( 'header > a' ) )
{    
    document.querySelector( 'header > a' ).onclick = function( e )
    {
        e.preventDefault();
        hashPage = 'home';
        duree = dureeTotalExercice;
    };
}

var textSupport = function( breath )
{
    if( isTextSupportOn )
    {
        if( document.querySelector( 'div.textsupport' ) )
        {
            document.querySelector( 'div.textsupport' ).innerHTML = breath;
            document.querySelector( 'div.textsupport' ).classList.add('appear');

            setTimeout(function()
            {
                if( document.querySelector( 'div.textsupport' ) )
                {
                    document.querySelector( 'div.textsupport' ).classList.remove('appear');
                }
            }, ( dureeInspiration / 6 * soufflesParMin - 2500 ) );
        }
    }  
};

var expireMeterOut = function()
{
    if( document.querySelector( 'div.particulemeterSmall' ) )
    {
        document.querySelector( 'div.particulemeterSmall' ).classList.remove( 'opacity' );
    }
};

var expireMeter = function()
{        
    if( document.querySelector( 'div.particulemeterSmall' ) )
    {
        document.querySelector( 'div.particulemeterSmall' ).classList.add( 'opacity' );
    }
    setTimeout( function(){ expireMeterOut(); }, 1750 );
};


var inspireMeterOut = function()
{
    if( document.querySelector( 'div.particulemeterBig' ) )
    {
        document.querySelector( 'div.particulemeterBig' ).classList.remove( 'opacity' );
    }
};

var inspireMeter = function()
{     
    clearInterval( delayStratAnim );

    if( document.querySelector( 'div.particulemeterBig' ) )
    {
        document.querySelector( 'div.particulemeterBig' ).classList.add( 'opacity' );
    }
    setTimeout( function(){ inspireMeterOut(); }, 1750 ); 
};

/**
 * Breath animation
 * n1 is Inspiration
 * n2 is Expiration
 * n3 is Waiting
 * @returns void
 */  
var animcycle = function()
{
    var n1 = 0;
    var n2 = 0;
    var n3 = 0;

    particulesPositions.forEach( function( v, k )
    {    
        var o = particulesPosition[k].obj;

        n1++;
        var l = particulesPosition[k].posLeft;
        var t = particulesPosition[k].posTop;
        var a = particulesPosition[k].animdist;

        if( n1 === 1 )
        {   
            if( isSoundOn && document.querySelector('audio#audio_loop1') && document.querySelector('audio#audio_loop2') )
            {
                var track = ( soundTrack % 2 === 0 ) ? 'audio_loop2' : 'audio_loop1';
                soundTrack++;
                document.querySelector('audio#'+track).currentTime = 0;        // AUDIO reset & PLAY
                document.querySelector('audio#'+track).play();
            }

            textSupport( 'Inspire' );

            animExpire = setTimeout( function(){ inspireMeter(); }, ( dureeExpiration * 0.5 ) + dureeAttente ); 
        }

        /*TweenMax*/
        TweenMax.to(o, (dureeInspiration/1000), {left:( ( l * animdist ) * a / 60 ), top:( ( t * animdist ) * a / 60 ), ease: Power2.aeseInOut, onComplete:function()
        {
            n2++;
            if( n2 === 1 )
            {   
                textSupport( 'Expire' );

                animInspire = setTimeout( function(){ expireMeter(); }, ( dureeInspiration * 0.5 ) + dureeAttente );
            }

            /*TweenMax*/
            TweenMax.to(o, (dureeExpiration/1000), {left:l, top:t, ease: Power2.aeseInOut, onComplete:function()
            {
                n3++;
                if( n3 === 1 && animIsOn )
                { 

                    animAttente = setTimeout( function(){ animcycle(); }, ( dureeAttente * 0.5 ) + dureeAttente );
                }
            }
            });

        }
        });
    });
};


/**
 * It fades the sounds
 * Used at the end of the breath animation
 * 
 * @returns void
 */
var soundFadeOut = function()
{
    soundVolume -= 0.01;

    if( soundVolume >= 0 )
    {
        if( document.querySelector('audio#audio_bkg') )
        {
            document.querySelector('audio#audio_bkg').volume = soundVolume;
        }
        if( document.querySelector('audio#audio_loop1') )
        {
            document.querySelector('audio#audio_loop1').volume = soundVolume;
        }
        if( document.querySelector('audio#audio_loop2') )
        {
            document.querySelector('audio#audio_loop2').volume = soundVolume;
        }
    }
    else
    {
        clearInterval( intervalSoundFadeOut );
        if( document.querySelector('audio#audio_bkg') )
        {
            document.querySelector('audio#audio_bkg').volume = 0;
        }
        if( document.querySelector('audio#audio_loop1') )
        {
            document.querySelector('audio#audio_loop1').volume = 0;
        }
        if( document.querySelector('audio#audio_loop2') )
        {
            document.querySelector('audio#audio_loop2').volume = 0;
        }
    }
};

/**
 * The exercice manager. 
 * Starts Counter (background animation one), Sound and Breath animation. 
 * Ends the process when counting is done.
 * 
 * @returns void
 */
var doExercice = function()
{
    duree += 1/60;

    if( duree < dureeTotalExercice )
    {
        if( isSoundOn && ( dureeTotalExercice - duree ) < isFadingOutDuration && !isFadingOut )
        { 
            isFadingOut = true;
            intervalSoundFadeOut = setInterval( function(){ soundFadeOut(); }, ( isFadingOutDuration * 600 ) );
        }
        setTimeout( function(){ doExercice(); }, 1000 );
    }
    else
    {        
        Array.prototype.forEach.call( particules, function( particule, k )
        {
            particule.classList.add('fadeout');
        });
        if( document.querySelector( 'div.countdown') )
        {
            document.querySelector( 'div.countdown').classList.remove('transparent');
        }

        if( document.querySelector( 'div.textsupport' ) )
        {
            document.querySelector( 'div.textsupport' ).classList.remove('appear');
        }

        setTimeout( function()
        { 
            if( document.querySelector( 'div.particulemeter' ) )
            {
                document.querySelector( 'div.particulemeter' ).classList.remove( 'opacity' ); 
            }
            if( document.querySelector( 'div.duree' ) )
            {
                document.querySelector( 'div.duree' ).style.width = '0px';
            }
        }, 500 );

        setTimeout( function()
        { 
            animIsOn = false; 
            location.hash = hashPage; 
        }, 2000 );
    } 
};

var setAudio = function( AmbienceSoundFile, BreathSoundFile )
{
    if( BreathSoundFile.length > 0 && AmbienceSoundFile.length > 0 )
    {
        document.querySelector( 'audio#audio_loop1 source' ).src = 'public/medias/audio/' + BreathSoundFile + '_1.mp3';
        document.querySelector( 'audio#audio_loop2 source' ).src = 'public/medias/audio/' + BreathSoundFile + '_2.mp3';
        document.querySelector( 'audio#audio_bkg source' ).src = 'public/medias/audio/' + AmbienceSoundFile + '.mp3';
        document.querySelector( 'audio#audio_loop1' ).load();
        document.querySelector( 'audio#audio_loop2' ).load();
        document.querySelector( 'audio#audio_bkg' ).load();
        document.querySelector( 'audio#audio_bkg' ).play(); // AUDIO loop1 PLAYS
    }
};

/**
 * The countdown before the breath animation begins
 * 
 * @returns void
 */
var doCountdown = function()
{
    if( countdown >= 1 )
    {
        document.querySelector( 'div.countdown > span' ).classList.remove('semitransparent');
        document.querySelector( 'div.countdown > span' ).innerHTML = countdown;
    }
    countdown--;

    if( countdown >= 0 )
    {
        setTimeout( function(){ 

            if( countdown >= 1 )
            {
                document.querySelector( 'div.countdown > span' ).classList.add('semitransparent');
                setTimeout(function(){ doCountdown(); }, 100);
            }
            else
            {
                document.querySelector( 'div.countdown > span' ).classList.add('transparent');
                setTimeout(function(){ doCountdown(); }, 100);
            }
        }, 1000); 
    }
    else
    {
        document.querySelector( 'div.countdown' ).classList.add('transparent');
        setTimeout(function(){ 

            if( isCountdownOn )
            {
                document.querySelector('div.duree').style.transitionDuration = dureeTotalExercice * 60 + 's'; 
                document.querySelector('div.duree').classList.add('animated');
            }
            doExercice();

            animcycle();
        }, 500);
    } 
};


/**
 * Sets all parameters (size, distance, start and end status) for the particules on the loading page
 * 
 * @returns void
 */
var animateParticules = function()
{
    animIsOn = true;

    duree = 0;
    particules              = null;
    intervalSoundFadeOut    = null;

    if( document.querySelector('div.particulemeter.particulemeterSmall') )
    {
        document.querySelector('div.particulemeter.particulemeterSmall').parentNode.removeChild(document.querySelector('div.particulemeter.particulemeterSmall'));
    }
    
    if( document.querySelector('div.particulemeter.particulemeterBig') )
    {
        document.querySelector('div.particulemeter.particulemeterBig').classList.remove('particulemeterBig');
    }
    isCountdownOn   = ( getSession( 'countdown' ) && ( getSession( 'countdown' ) === 'true' || getSession( 'countdown' ) === true ) ) ? true : false;
    isSoundOn       = ( getSession( 'sound' ) && ( getSession( 'sound' ) === 'true' || getSession( 'sound' ) === true ) ) ? true : false;
    isTextSupportOn = ( getSession( 'textsupport' ) && ( getSession( 'textsupport' ) === 'true' || getSession( 'textsupport' ) === true ) ) ? true : false;

    var amplitudeMax = 200; // Distance limite entre les points (inspire à expire)

    var wWidth  = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    var wHeight = window.innerHeight|| document.documentElement.clientHeight || document.body.clientHeight;

    var animdistMax = ( wWidth < wHeight )                  ? wWidth        : wHeight;
    animdist        = ( animdistMax * 0.25 > amplitudeMax ) ? amplitudeMax  : animdistMax * 0.25;

    compressAmplitude  = animdist / amplitudeMax;
    expendAmplitude    = amplitudeMax / animdist;

    particulemeterSize = animdist * 4 * 0.9 ; // 90% de la fenetre

    if( !particuleIsSet )
    {   
        particulesPositions.forEach( function( v, k )
        {
            var particule = document.createElement('span');
            particule.dataset.posleft = v[0];
            particule.dataset.postop = v[1];

            particule.style.width = ( particulesSize * compressAmplitude * 1.45 ) + 'px';
            particule.style.height = ( particulesSize * compressAmplitude * 1.45 ) + 'px';
            particule.style.marginLeft = '-' + ( particulesSize * compressAmplitude * 0.725  ) + 'px';
            particule.style.marginTop = '-' + ( particulesSize * compressAmplitude * 0.725 ) + 'px';
            document.querySelector('div.particules').appendChild(particule);
        });

        particules = document.querySelectorAll( 'div.particules > span' );

        document.querySelector('div.particulemeter').setAttribute('style', 'width:' + ( 200 * compressAmplitude + 26 ) + 'px; height:' + ( 200 * compressAmplitude + 26 ) + 'px; border-width:3px' );
        document.querySelector('div.particulemeter').classList.add('particulemeterSmall');

        var particulemeterSmall = document.querySelector('div.particulemeter').cloneNode(true);
        particulemeterSmall.setAttribute('style', 'width:' + particulemeterSize + 'px; height:' + particulemeterSize + 'px; border-width:5px');
        particulemeterSmall.classList.remove('particulemeterSmall');
        particulemeterSmall.classList.add('particulemeterBig');
        document.querySelector('main').appendChild( particulemeterSmall );
    }  
    else
    {
        document.querySelector('div.particulemeter.particulemeterBig').setAttribute('style', 'width:' + particulemeterSize + 'px; height:' + particulemeterSize + 'px');
        document.querySelector('div.particulemeter.particulemeterSmall').setAttribute('style', 'width:' + ( 200 * compressAmplitude + 26 ) + 'px; height:' + ( 200 * compressAmplitude + 26 ) + 'px');
    }

    Array.prototype.forEach.call( particules, function( particule, k )
    {  
        var random  = Math.floor((Math.random() * 2));
        var random2  = Math.floor((Math.random() * 3));
        var posLeft = parseInt( particule.dataset.posleft );
        var posTop  = parseInt( particule.dataset.postop );

        var posX = ( Math.abs( posTop ) +  Math.abs( posLeft ) );

        var animdistFactor = 1.0 * expendAmplitude;

        posLeft *= compressAmplitude;
        posTop  *= compressAmplitude; 

        if( k % random !== 0 )
        {
            if( 20 > posX ){ animdistFactor = 0.5 * expendAmplitude; }
            else if( 40 > posX ){ animdistFactor = 0.6 * expendAmplitude; }
            else if( 60 > posX ){ animdistFactor = 0.7 * expendAmplitude; }
            else if( 80 > posX ){ animdistFactor = 0.8 * expendAmplitude; }
            else if( 100 >= posX ){ animdistFactor = 0.9 * expendAmplitude; }
        }

        particulesPosition[k] = {obj:particule, posLeft:posLeft, posTop:posTop, animdist:animdistFactor, animate:random2};
        particule.style.left = particulesPosition[k].posLeft + 'px';
        particule.style.top = particulesPosition[k].posTop + 'px';
    });

    if( !particuleIsSet )
    { 
        if( isSoundOn )
        {
            if( getSession( 'ambience' ) && getSession( 'ambience' ).length > 0 && getSession( 'breath' ) && getSession( 'breath' ).length > 0 )
            {
                AmbienceSoundFile = getSession( 'ambience' );
                BreathSoundFile   = getSession( 'breath' );

                setAudio( AmbienceSoundFile, BreathSoundFile );
            }
            else
            {
                var request = new XMLHttpRequest();

                request.open( 'GET', 'public/json/sounds.json', true );

                request.onload = function() 
                {
                    if( request.status >= 200 && request.status < 400 )
                    {
                        var jsonAudios = JSON.parse( request.responseText );

                        AmbienceSoundFile = jsonAudios.ambience[0][0];
                        BreathSoundFile   = jsonAudios.breath[0][0];

                        setAudio( AmbienceSoundFile, BreathSoundFile );
                    }
                };

                request.send();
            }

        }
        doCountdown();
    }

    particuleIsSet = true;
}; 

animateParticules();

window.onresize = function()
{
    animateParticules();
};