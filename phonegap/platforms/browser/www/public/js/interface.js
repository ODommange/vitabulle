var soundList = function( parentMinified )
{
    var soundsJsonFile = ['ambience', 'breath'];

    var request = new XMLHttpRequest();

    request.open( 'GET', 'public/json/sounds.json', true );

    request.onload = function() 
    {
        if( request.status >= 200 && request.status < 400 )
        {
            var jsonAudios = JSON.parse( request.responseText );
            
            Array.prototype.forEach.call( soundsJsonFile, function( soundInfoName )
            {
                document.querySelector( 'ul#' + soundInfoName ).innerHTML = '';
                
                jsonAudios[ soundInfoName ].forEach( function( sound, index )
                {
                    var li          = document.createElement('li');
                    var span        = document.createElement('span');
                    var spanButton  = document.createElement('span');
                    var button      = document.createElement('button');

                    li.setAttribute( 'data-sound', sound[0] );
                    span.innerHTML = sound[1] + ' <svg width="20" height="20" viewBox="0 0 15 15" fill="#ffffff" xmlns="http://www.w3.org/2000/svg"><path d="M4.7,0.4L15,7.5L4.7,14.6V0.4z" fill=""/> </svg>';

                    button.appendChild( spanButton );
                    li.appendChild( span );
                    li.appendChild( button );

                    document.querySelector( 'ul#' + soundInfoName ).appendChild( li );
                    
                    document.querySelector( 'ul#' + soundInfoName + ' li:nth-child( ' + (index + 1) + ' ) button' ).onclick = function()
                    {
                        setOnChecked( soundInfoName, this.parentNode.dataset.sound );
                    };
                    
                    document.querySelector( 'ul#' + soundInfoName + ' li:nth-child( ' + (index + 1) + ' ) > span' ).onclick = function()
                    {
                        var extension = ( soundInfoName === 'breath' ) ? '_1' : '';
                        playSound( this.parentNode.dataset.sound + extension );
                    };
                    
                    parentMinified.classList.add( 'open' );
                    parentMinified.classList.remove( 'close' ); 
                });

                setOnChecked( soundInfoName, false );
            });
        }
    };

    request.send();
};


var playSound = function( soundFileName )
{
    document.querySelector( 'audio#soundcheck source' ).src = 'public/medias/audio/' + soundFileName + '.mp3';
    
    document.querySelector( 'audio#soundcheck' ).load();
    
    document.querySelector( 'audio#soundcheck' ).currentTime = 0;  
    
    document.querySelector( 'audio#soundcheck' ).play();
};


/**
 * 
 * @param {type} parameterName
 * @returns {undefined}
 */
var setOnChecked = function( checkList, soundFileName )
{
    if( document.querySelector( 'ul#' + checkList ) )
    {
        if( soundFileName )
        {
            doChecked( checkList, soundFileName );
        }
        else if( getSession( checkList ) )
        {
            doChecked( checkList, getSession( checkList ) );
        }
        else
        {    
            doChecked( checkList, document.querySelector( 'ul#' + checkList + ' li:first-child' ).dataset.sound );           
        }
    }
};


var doChecked = function( checkList, soundFileName )
{
    setSession( checkList, soundFileName );
    
    var lisCheck = document.querySelectorAll( 'ul#' + checkList + ' li' );
    Array.prototype.forEach.call( lisCheck, function( el )
    {
        var soundFile = el.dataset.sound;
        if( soundFileName === soundFile )
        {
            el.querySelector('button').classList.add( 'checked' );
        }
        else
        {
            el.querySelector('button').classList.remove( 'checked' );
        }
    });
};

/**
 * 
 * @param {type} parameterName
 * @returns {undefined}
 */
var setOnParameter = function( parameterName )
{
    if( document.querySelector( 'button[data-action="' + parameterName + '"]') )
    {
        if( getSession( parameterName ) && ( getSession( parameterName ) === 'true' || getSession( parameterName ) === true ) )
        {
            document.querySelector( 'button[data-action="' + parameterName + '"]').classList.add( 'on' );
            document.querySelector( 'button[data-action="' + parameterName + '"]').classList.remove( 'off' );
        }
        else
        {
            document.querySelector( 'button[data-action="' + parameterName + '"]').classList.remove( 'on' );
            document.querySelector( 'button[data-action="' + parameterName + '"]').classList.add( 'off' ); 
        }
    }
};






   /**
    * Opens or closes the parameter Menu
    */
   if( document.querySelector( '.palette > div' ) )
   {
        document.querySelector( '.palette > div' ).onclick = function()
        {
            if( document.querySelector( 'body' ).classList.contains( 'menu-on' ) )
            {
                document.querySelector( 'header menu' ).style.position = 'fixed';
                document.querySelector( 'body' ).classList.remove( 'menu-on' ); 
            }
            else
            {
                document.querySelector( 'body' ).classList.add( 'menu-on' );
                setTimeout( function(){ document.querySelector( 'header menu' ).style.position = 'absolute'; }, 1000);
                
            }
        };     
    }
    
    setOnParameter( 'sound' );
    
    setOnParameter( 'countdown' );
    
    setOnParameter( 'textsupport' );
    
    
    /**
     * 
     * @type NodeList
     */
    var checkBoxButtons = document.querySelectorAll('.check button');
    
    Array.prototype.forEach.call( checkBoxButtons, function( el )
    {
        el.onclick = function( e )
        {
            e.preventDefault();
            if( this.classList.contains('on') )
            {
                this.classList.add('off');
                this.classList.remove('on');

                removeSession( this.dataset.action );
            }
            else
            {
                this.classList.add('on');
                this.classList.remove('off');

                setSession( this.dataset.action, 'true');
            }
        };
    });

    /**
     * 
     * @param {type} e
     * @returns {undefined}
     */
    var minifiedButtons = document.querySelectorAll( '.minified' );
    Array.prototype.forEach.call( minifiedButtons, function( el )
    {
        el.onclick = function( e )
        {
           e.preventDefault();
            var parentMinified = this.parentNode;
            
            if( parentMinified.querySelector( '.minified-list' ) )
            {
                if( parentMinified.classList.contains( 'open' ) )
                {
                    parentMinified.classList.add( 'close' );
                    parentMinified.classList.remove( 'open' ); 
                }
                else
                {
                    if( el.id === 'sounds' )
                    {
                        soundList( parentMinified );
                    }
                    else
                    {
                        parentMinified.classList.add( 'open' );
                        parentMinified.classList.remove( 'close' ); 
                    }
                }
            }
        };
    });
    
    /**
     * 
     * @type NodeList
     */
    var colorsButtons = document.querySelectorAll( 'a[data-color]' );
    Array.prototype.forEach.call( colorsButtons, function( el )
    {
        el.onclick = function( e )
        {
            e.preventDefault();

            setSession( 'color', this.dataset.color );

            document.querySelector('body').classList.remove('blue', 'red', 'grey', 'beige', 'green');

            document.querySelector('body').classList.add( this.dataset.color );

            document.querySelector( '.menu-color li' ).classList.remove( 'actif' );

            this.parentNode.classList.add( 'actif' );
        };
    });


    /**
     * 
     * @param {type} e
     * @returns {undefined}
     */
    if( document.querySelector( '[data-action="logout"]' ) )
    {
        document.querySelector( '[data-action="logout"]' ).onclick = function( e )
        {
            e.preventDefault();

            logout();
        };
    }
    
    /**
     * 
     */
    if( document.querySelector( 'footer nav a[href="#exercice"]' ) )
    {
        document.querySelector( 'footer nav a[href="#exercice"]' ).onclick = function( e )
        {
            e.preventDefault();
            if( !getSession('warning') )
            {
                document.querySelector( 'div#warningcc.modal' ).classList.add( 'drop' );    // Animate with CSS... transition:top 1s;
                setSession('warning', true);
            }
            else
            {
                location.hash = 'exercice';
            }
        };
    }
    
    /**
     * 
     */
    if( document.querySelector( 'a#passforgot' ) )
    {
        document.querySelector( 'a#passforgot' ).onclick = function( e )
        {
            e.preventDefault();
            document.querySelector( 'div#gogetpassonline.modal' ).classList.add( 'drop' );
        };
    }
    
    
