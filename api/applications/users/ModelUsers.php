<?php  
namespace applications\users;

use includes\components\CommonModel;

use includes\tools\Orm;
use includes\tools\Date;
use includes\tools\Mail;
use includes\Request;
use includes\Login;
  
/**
 * class Model
 * 
 * Filters apps datas
 *
 * @param array $_beneficiaire  | Table and fields structure "users".
 *                  
 */
class ModelUsers extends CommonModel {     
    
    
    function __construct() 
    {
        $this->_setTables(['users/builders/BuilderUsers']);
        
        $this->_setModels([ 'users/ModelGroups' ]);
        
    }

    /**
     * Select datas form the table "users"
     * 
     * @param array $params | (optional) Conditions [ 'Field'=>value ]
     * @param str   $period | (optional) Period or state depending on value choosed
     *                        ('all', 'archive', 'actual', 'future', 'cancel', search, or integer(year-YYYY))
     *                        'all' by default
     * @param array $groups | (optional) Group(s) Type ('participants' or 'manager') or 'all' (for all groups)
     * @return array        | Results of the selection in the database.
     */
    public function users( $params = [] )
    {
        $orm = new Orm( 'users', $this->_dbTables['users'], $this->_dbTables['relations'] );

        $result = $orm    ->select()
                ->joins( ['users'=>['groups']] )
                ->where( $params )
                ->execute( true );
        
        return $result;
    }
    
    
    /**
     * Prepare datas for the formulas 
     * depending on the table "beneficiaire".
     * Manage sending. Returns settings datas and errors
     * 
     * @param int $id       | (optional) Id of the content. 
     * @return object       | Datas and errors.
     */   
    public function userBuild( $id = null )
    {
        $orm = new Orm( 'users', $this->_dbTables['users'] );
            
        $orm->prepareGlobalDatas( [ 'POST' => true ] );
        
        $params = ( isset( $id ) ) ? ['IdUser' => $id] : null;
            
        return $orm->build( $params );
    }
    
    /**
     * Updates datas in the database.
     * Do insert and update.
     * Figure errors and send back false in that case
     * 
     * @param string $action  | (optionnal) Action to do.
     *                          Default : does insert.
     *                          Defined by "insert" or "update". 
     * @param int $id         | (optional) Id of the content to update.
     *                          It is mandatory for updates.
     * @return boolean|object | false when errors are found 
     *                          (ex. empty fields, bad file format imported,...). 
     *                          Object with content datas when process went good. 
     */ 
    public function userUpdate( $action = 'insert', $id = null) 
    {
        $orm        = new Orm( 'users', $this->_dbTables['users'] );
        
        $datas = $orm->prepareGlobalDatas( [ 'POST' => true ] );

        if( !$orm->issetErrors() )
        {
            if( $action === 'insert' )
            {
                $request        = Request::getInstance();
                $newpassword    = $request->genToken( 3 );
                        
                $orm->prepareDatas([ 'PassUser' => $newpassword, 'PseudoUser' => $datas['EmailUser'], 'IsAccountActivated' => 1 ]);
             
                $data = $orm->insert();                
                
                $id = $data->IdUser;
            }
            else if( $action === 'update' )
            {
                $data = $orm->update([ 'IdUser' => $id ]);
            }    
            
            return $data;
        }
        else
        {
            return false;
        }
    }

    
    /**
     * Delete an entry in the database.
     * 
     * @param int $id   | Id of the content to delete.
     * @return boolean  | Return's true in all cases.    
     */
    public function userDelete( $id ) 
    {
        $orm = new Orm( 'users', $this->_dbTables['users'] );
            
        $orm->delete([ 'IdUser' => $id ]);
        
        return true;
    } 
    
    
    private function _subscribeConfirmEmail( $emailUser )
    {
        Login::passdefine( $emailUser );
    }
    
    private function _subscribeCompleteEmail( $emailUser, $IdUser, $TokenUser )
    {
        if( isset( $emailUser ) )
        {
            $mail = new Mail();

            $message = 'Bonjour,<br />
                        Bienvenue dans l\'application &laquo;Vitabulle&raquo;.
                        Tu es invité à <a href="' . SITE_WEBSITE . '/users/validate/'. $IdUser . '/' . $TokenUser . '">valider ton profil utilisateur en cliquant sur ce lien</a>.<br /><br />'.
                        'Tu pourras ensuite te connecter à ton profil depuis l\'application Vitabulle.'.
                        '<br /><br />Cordialement.<br /><br />' . SITE_TITLE;
            $fromnom    = SITE_TITLE;
            $frommail   = SITE_EMAIL;

            $mail->sendSiteMail( $emailUser, 'Complète ton profil', $message, $fromnom, $frommail); 
            
            return true;
        }
    }
    
    
    //$datas[1-4] === timestamp/EmailUser/PassUser
    public function usersApiSubscribe( $datas )
    {
        $errors = [];
        
        if( count( $datas ) === 4 )
        {
            if( empty( $datas[2] ) )
            {
                $errors['EmailUserEmpty'] = true;            
            }
            else if( !filter_var( $datas[2], FILTER_VALIDATE_EMAIL ) ) 
            {
                $errors['EmailUserFormat'] = true;
            }
            else if( $this->users( ['EmailUser' => $datas[2] ]) ) 
            {
                $errors['EmailUserExist'] = true;
            }
            
            if( empty( $datas[3] ) )
            {
                $errors['PassUserEmpty'] = true;            
            }
            else if( strlen( $datas[3] ) <= 7 ) 
            {
                $errors['PassUserlong'] = true;
            }

            if( count( $errors ) === 0 )
            {
                $orm = new Orm( 'users', $this->_dbTables['users'] );

                $password = Login::applyUserCryptPass($datas[3]);

                $orm->prepareDatas([ 
                    'PseudoUser'    => $datas[2], 
                    'PassUser'      => $password, 
                    'IdGroup'       => 2,  
                    'EmailUser'     => $datas[2] 
                ]);

                $data = $orm->insert();
                
                $token = Login::applyUserCryptPass( $datas[2] . '-' . $data->IdUser );
                                
                $orm->prepareDatas([ 
                    'TokenUser'    => $token
                ]);
                
                $orm->update(['IdUser' => $data->IdUser ]);

                $this->_subscribeCompleteEmail( $datas[2], $data->IdUser, $token );

                return $this->usersApiLogin( $datas, 'subscribe' );
            }
            else
            {
                return [ 'process'=>'fail', 'errors'=>$errors ];
            }
        }
        else
        {
            return [ 'process'=>'fail', 'errors'=>['InvalidNbEntries' => true] ];
        }
    }
    
    
    // $datas[1-8] === timestamp/token/LastnameUser/FirstnameUser/EmailUser/01/01/1970
    public function usersApiSubscribeComplete( $datas )
    {
        $errors = [];
        
        if( count( $datas ) === 8 )
        {

            if( empty( $datas[4] ) )
            {
                $errors['EmailUserEmpty'] = true;            
            }
            else if( !filter_var( $datas[4], FILTER_VALIDATE_EMAIL ) ) 
            {
                $errors['EmailUserFormat'] = true;
            }

            $BirthdayUser = ( !empty( $datas[7] ) && is_numeric( $datas[7] ) && !empty( $datas[6] ) && is_numeric( $datas[6] ) && !empty( $datas[5] ) && is_numeric( $datas[5] ) && checkdate($datas[6], $datas[5], $datas[7]) ) ? $datas[7] . '-' . $datas[6] . '-'. $datas[5] : '0000-00-00';

            if( $BirthdayUser !== '0000-00-00' )
            {
                $date = new Date( $BirthdayUser, 'YYYY-MM-DD' );
                if( $date->get_time_difference( time() )['seconds'] < ( 12 * 365 * 24 * 3600 ) )
                {
                    $errors['AgeUserUnder12'] = true;
                }
            }

            if( count( $errors ) === 0 )
            {
                $orm = new Orm( 'users', $this->_dbTables['users'] );

                $request        = Request::getInstance();
                $newpassword    = $request->genToken( 3 );

                //$datas[1-8] === timestamp/token/LastnameUser/FirstnameUser/EmailUser/01/01/1970
                $orm->prepareDatas([ 
                    'PseudoUser'    => $datas[4], 
                    'PassUser'      => $newpassword, 
                    'IdGroup'       => 3, 
                    'LastnameUser'  => $datas[2], 
                    'FirstnameUser' => $datas[3], 
                    'BirthdayUser' => $BirthdayUser, 
                    'EmailUser'     => $datas[4] 
                ]);

                $data = $orm->insert();    
                
                $token = Login::applyUserCryptPass( $datas[2] . '-' . $data->IdUser );
                                
                $orm->prepareDatas([ 
                    'TokenUser'    => $token
                ]);
                
                $orm->update(['IdUser' => $data->IdUser ]);

                $this->_subscribeCompleteEmail( $datas[2], $data->IdUser, $token );

                return $this->usersApiLogin( $datas, 'subscribe' );
            }
            else
            {
                return [ 'process'=>'fail', 'errors'=>$errors ];
            }
        }
        else
        {
            return [ 'process'=>'fail', 'errors'=>['InvalidNbEntries' => true] ];
        }
    }
    
    
    public function usersApiLogin( $datas, $type = 'login' )
    {
        $user = Login::loguser( $datas[2], $datas[3] );
        
        if( isset( $user ) )
        {
            $token = Login::applyUserCryptPass( $datas[2] . '-' . $user->IdUser );
            
            if( $user->IsAccountActivated === '1' )
            {
                return [ 'process'=>'success', 'id'=>$user->IdUser, 'firstname'=>$user->FirstnameUser, 'lastname'=>$user->LastnameUser, 'email'=>$user->EmailUser, 'idgroup'=>$user->IdGroup, 'token'=>$token ];
            }
            else
            {
                return [ 'process'=>( $type === 'subscribe' ) ? 'success' : 'fail', 'errors'=>['accountNotActivated' => true], 'email'=>$user->EmailUser, 'id'=>$user->IdUser, 'token'=>$token ];
            }
        }
        else
        {
            return [ 'process'=>'fail', 'errors'=>['accountNotFound' => true] ];
        }
    }
    
    public function checkToken( $token, $id )
    {
        $Orm    = new Orm( 'users' );

        $user = $Orm   ->select()
                       ->where([ 'IdUser'=>$id, 'TokenUser'=>$token ])
                       ->first();
        
        return ( isset( $user ) ) ? $user : false;        
    }
    
    
    public function activate( $user )
    {
        $token = Login::applyUserCryptPass( $user->EmailUser . '-' . $user->IdUser );
        
        $this->_subscribeCompleteEmail( $user->EmailUser, $user->IdUser, $token );
    }
    
    
    public function usersApiDatas( $apiDatas ) 
    {
        $usersApi = [];
        
        if( is_array( $apiDatas ) && count( $apiDatas ) >= 2 && is_numeric( $apiDatas[ 2 ] ) )
        {
            $users = $this->users( ['IdUser' => $apiDatas[2], 'TokenUser' => $apiDatas[1] ] );

            if( isset( $users ) )
            {
                foreach( $users as $u )
                {
                    $fields = [];

                    foreach( $u as $prop => $value )
                    {
                        if( is_string( $value ) && $prop !== 'PassUser' && $prop !== 'TokenUser' && $prop !== 'EmailUser' && $prop !== 'PhoneUser' && $prop !== 'AddressUser' && $prop !== 'ZipCodeUser' )
                        {
                            $fields[ $prop ] = $value;
                        }
                    }
                    $fields[ 'todayStats' ] = $this->statsApiDays(['IdUser' => $u->IdUser ], date('Y-m-d'));

                    $usersApi[]  = $fields;
                }
            }
        }

        return $usersApi;
    } 

    
    public function stats( $params, $paramGreater = [], $paramLower = [] )
    {
        $orm = new Orm( 'users_statistics', $this->_dbTables['users_statistics'] );

        $results = $orm ->select()
                        ->where( $params )
                        ->wheregreaterandequal( $paramGreater )
                        ->wherelowerandequal( $paramLower )
                        ->order([ 'DateStatistic' => 'ASC' ])
                        ->execute();
        
        return $results;
    }
    
    
    public function statsApiDays( $params, $days = 'all' ) 
    {
        $statsApi       = [];
        
        $paramGreater   = ( $days !== 'all' ) ? [ 'DateStatistic' => $days . ' 00:00:00' ] : [];
        
        $paramLower     = ( $days !== 'all' ) ? [ 'DateStatistic' => $days . ' 23:59:59' ] : [];
        
        $results = $this->stats( $params, $paramGreater, $paramLower );
        
        if( isset( $results ) )
        {
            foreach( $results as $result )
            {
                if( !empty( $result->DateStatistic ) && $result->DateStatistic !== '0000-00-00 00:00:00' )
                {
                    $date = new Date( $result->DateStatistic );

                    $dayDateHyphen = $date->get_date_hyphen( 'DD-MM-YYYY' );

                    if( !isset( $dayDateHyphen ) ) 
                    {
                        $statsApi[ $dayDateHyphen ] = [];
                    }

                    $time = explode( ':', $date->get_time() );
                    
                    $statsApi[] = [ 'date'=>$date->get_date(), 'hour'=>$time[0], 'min'=>$time[1], 'eval'=>$result->ValueStatistic ];
                }
            }
        }
        
        return $statsApi;
    } 

    /**
     * 
     * @param array $params
     * @param integer $month
     * @return array
     */
    public function statsApiMonth( $params, $month, $year ) 
    {
        $statsApi = [];
        
        $dateEndTimestamp = mktime( '00', '00', '00', ( $month + 1 ), '01', $year );
        
        $dayEndMonth = date( 'd', ($dateEndTimestamp - ( 3600 * 24 ) ) );
        
        for( $i = 1; $i <= $dayEndMonth; $i++ )
        {
            $day = ( $i <= 9 ) ? '0'.$i : $i;
            
            $statsApi[] = [ 'date' =>  $year . '-' . $month . '-' . $day, 'values' => $this->statsApiDays( $params, $year . '-' . $month . '-' . $day ) ];
        }
        
        return $statsApi;
    } 
    
    
    public function statsApiMonthByWeek( $params, $month, $year ) 
    {
        $statsApi = [];
        
        $dateBeggingTimestamp = mktime( '00', '00', '00', ( $month ), '01', $year );
        
        $weekBeggingMonth = date( 'W', ($dateBeggingTimestamp ) );
        
        $dateEndTimestamp = mktime( '00', '00', '00', ( $month + 1 ), '01', $year );
        
        $weekEndMonth = date( 'W', ($dateEndTimestamp - ( 3600 * 24 ) ) );
        
        for( $i = $weekBeggingMonth; $i <= $weekEndMonth; $i++ )
        {            
            $statsApi[] = [ 'week' =>  $i, 'dates' => $this->statsApiWeek( $params, $i, $year ) ];
        }
        
        return $statsApi;
    } 

    

    public function statsApiWeek( $params, $week, $year )
    {
        $statsApi = [];
        
        $dateStartTimestamp = strtotime( sprintf("%4dW%02d", $year, $week ) ); // Start of the week
        
        $dayLength = ( 3600 * 24 );
        
        $limit = $dateStartTimestamp + ( 6 * $dayLength );
        
        for( $i = $dateStartTimestamp; $i <= $limit; $i += $dayLength )
        {
            $date = date( 'Y-m-d', $i );
            $day = date( 'w', $i );
            
            $statsApi[] = [ 'date' => $date, 'day' => $day, 'values' => $this->statsApiDays( $params, $date ) ];
        }
        
        return $statsApi;
    }

    /**
     * 
     * @param type $params
     * @param type $year
     * @return type
     */
    public function statsApiYear($params, $year)
    {
        $statsApi = [];
        
        for( $i = 1; $i <= 12; $i++ )
        {
            $monthDays = $this->statsApiMonth( $params, $i, $year );
            
            $daysEval = 0;
            $daysEvalTotal = 0;
            foreach( $monthDays as $day )
            {
                $nbEvals = count( $day['values'] );
                
                $daysEval       += ( $nbEvals > 3 ) ? 3 : $nbEvals;
                $daysEvalTotal  += $nbEvals;
            }
            
            
            $statsApi[] = [ 'month' => $i, 'nbDays' => count( $monthDays ), 'nbEval' => $daysEval, 'nbEvalTotal' => $daysEvalTotal ];
        }
        
        return $statsApi;
    }

    /**
     * 
     * @param type $params
     * @param type $from
     * @param type $to
     * @param type $returnFormat 'days', 'weeks' or 'months'
     * @return type
     */
    public function statsApiFromto($params, $from, $to, $returnFormat)
    {
        $statsApi = [];
        $dateFrom   = new Date($from, 'YYYY-MM-DD');
        $dateTo     = new Date($to, 'YYYY-MM-DD');
        
        for( $d = $dateFrom->get_timestamp(); $d <= $dateTo->get_timestamp(); $d += 86400 )
        {
            $date = date( 'Y-m-d', $d );
            
            $statsApi[] = $this->statsApiDays( $params, $date );
            
        }
        
        return $statsApi;
    }
    
    /**
     * 
     * @param type $IdUSer
     * @param type $eval
     * @param integer $datetime timestamp
     */
    public function statsApiInsert($IdUSer, $eval, $timestamp)
    {
        $usersApi = [];
        
        $time = $timestamp - 120;

        $dateStatisticLimit = date( 'Y-m-d H:i:s', $time );
        
        $results = $this->stats( ['IdUser' => $IdUSer ], [ 'DateStatistic' => $dateStatisticLimit ] );
      
        if( !isset( $results ) )
        {
            $dateStatistic = date( 'Y-m-d H:i:s', $timestamp );

            $orm = new Orm( 'users_statistics', $this->_dbTables['users_statistics'] );
            
            $datas = ['IdUser'=>$IdUSer, 'ValueStatistic'=>$eval, 'DateStatistic'=>$dateStatistic ];
                        
            $orm->prepareDatas($datas);

            $data = $orm ->insert();
            
            if( isset( $data ) )
            {
                foreach( $data as $prop => $value  )
                {
                    if( $prop != 'field')
                    {
                        $usersApi[ $prop ] = $value;
                    }
                }
                $usersApi['process'] = 'success';
            }
            else
            {
                $usersApi['process'] = 'fail';
            }
            
        }
        
        return $usersApi;
            
    }
    
}