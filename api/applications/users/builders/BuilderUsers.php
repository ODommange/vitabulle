<?php

return [
    
    /**
     * Fields format used by the Orm
     */
    'users' => [
        'IdUser'       =>[ 'type' => 'INT', 'autoincrement' => true, 'primary' => true ],
        'PseudoUser'   =>[ 'type' => 'STR' ],
        'PassUser'     =>[ 'type' => 'STR' ],
        'IdGroup'      =>[ 'type' => 'INT' ],
        'LastnameUser' =>[ 'type' => 'STR', 'default' => '' ],
        'FirstnameUser'=>[ 'type' => 'STR', 'default' => '' ],
        'BirthdayUser' =>[ 'type' => 'DATE', 'default' => '0000-00-00' ],
        'EmailUser'    =>[ 'type' => 'STR', 'mandatory' => true ],
        'PhoneUser'   =>[ 'type' => 'STR', 'default' => '' ],
        'AddressUser' =>[ 'type' => 'STR', 'default' => '' ],
        'ZipCodeUser' =>[ 'type' => 'INT', 'default' => '' ],
        'CityUser'    =>[ 'type' => 'STR', 'default' => '' ],
        'ParamsUser'  =>[ 'type' => 'STR', 'default' => '' ],
        'TokenUser'   =>[ 'type' => 'STR', 'default' => 'hastobe' ],
        'IsAccountActivated' =>[ 'type' => 'INT', 'default' => '0' ],
    ],
    
    'users_statistics' => [
        'IdStatistic'    =>[ 'type' => 'INT', 'autoincrement' => true, 'primary' => true ],
        'IdUser'         =>[ 'type' => 'INT' ],
        'ValueStatistic' => [ 'type' => 'INT' ],
        'DateStatistic' => [ 'type' => 'STR' ]
    ],
       
    /**
     * Jointure between tables by the foreign keys. Used by the Orm
     */
    'relations' => [
        'users' => [
            'groups' =>['users'=>'IdGroup', 'groups'=>'IdGroup']
        ]
    ]
    
];

