var wHeight = window.innerHeight;

var expand = document.querySelector( 'div.calendar-bkg' ).getBoundingClientRect().top + document.body.scrollTop;

var wHeight = window.innerHeight|| document.documentElement.clientHeight || document.body.clientHeight;

//document.querySelector( 'section#today' ).style.minHeight = ( wHeight - document.querySelector( 'section#today' ).getBoundingClientRect().top ) + 'px';
document.querySelector( 'div.calendar-bkg' ).style.minHeight = ( wHeight - document.querySelector( 'div.calendar-bkg' ).getBoundingClientRect().top ) + 'px';

var tips = {
    "rythm":"Espace tes séances en laissant quatre heures entre chacune d'elles. Les effets bénéfiques de la Cohérence cardiaque peuvent durer jusqu'à quatre heures.",
    "tomany":"Comme pour toute bonne chose, il ne faut pas abuser. Les médecins recommandent une pratique quotidienne de la Cohérence cardiaque trois fois par jour. Il est vivement recommandé de ne pas aller au-delà de quatre fois par jour.",
    "notenough":"Pour bénéficier d'un meilleur effet, tu devrais respecter un rythme régulier de trois séances par jour.",
    "notefficient":"L'efficacité des séances peut être optimisée si tu pratiques dans un lieu tranquille."
};

var practices   = [];
var eval        = 0;
var evalRythm   = 0;
var nbPractices = datas.apiDatas.length;

 for( var p = 0; p < nbPractices; p++ )
{
    practices[ p ] = datas.apiDatas[p].hour;

    evalRythm += ( p === 0 || ( p > 0 && ( parseInt( practices[ ( p ) ] ) - parseInt( practices[ p - 1 ] ) ) > 3 ) ) ? 1 : 0; // if 3 hours in between +1

    eval += parseInt( datas.apiDatas[p].eval ); // add each evaluation points
}   

var tipsKey = '';

if( nbPractices >= 4 )
{
    tipsKey = "tomany";
}
else if( nbPractices <= 1 )
{
    tipsKey = "notenough";
}
else if( evalRythm < nbPractices )
{
    tipsKey = "rythm";
}
else if( ( eval / nbPractices ) < 2 )
{
    tipsKey = "notefficient";
}

if( tipsKey !== '' )
{
    document.querySelector( 'a#advicebtn' ).onclick = function( e )
    {
        e.preventDefault();
        
        this.querySelector( 'div' ).innerHTML = tips[tipsKey];
        this.querySelector( 'div' ).classList.add( 'open' );
    };
}
else
{
    document.querySelector( 'a#advicebtn' ).style.display = 'none';
}