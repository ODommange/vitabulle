/**
 * Events (focus, keypress) sets on fields
 * 
 * @param {array} fields  Fields (objects) that are concerned
 * @param {string} traitmentName  Use to be 'login' or 'subscribe'
 * @returns void
 */
var setEventsOnFields = function( fields, traitmentName )
{
    if( fields && fields.length > 0 )
    {
        Array.prototype.forEach.call( fields, function( field )
        {
            if( field )
            {
                field.addEventListener( 'focus', function()
                {
                    field.nextElementSibling;
                    if( field.nextElementSibling.classList.contains( 'open' ) )
                    {
                        field.nextElementSibling.classList.add( 'close' );
                        field.nextElementSibling.classList.remove( 'open' );
                    }
                    if( document.querySelector( 'div#error-connect' ) )
                    {
                        document.querySelector( 'div#error-connect' ).classList.add( 'close' );
                        document.querySelector( 'div#error-connect' ).classList.remove( 'open' );
                    }
                }, true);

                field.onkeydown = function( event )
                {
                    if ( event.which === 13 ) 
                    {
                       event.preventDefault();

                       formDatasSend( traitmentName );
                    }
                };
            }
        });
    }
};

/**
 * When an account already exist but is not activated, this process is activated 
 * so the user can initiate the process of activation once again.
 * @param {object} userDatas  Object of datas coming from the DB throught the API
 * @returns void
 */
var setActiveAccount = function( userDatas )
{    
    var btnValidate = document.createElement('button');

    btnValidate.classList.add( 'btn', 'btnDark' );

    document.querySelector('div#formactivate').appendChild( btnValidate );

    document.querySelector('div#formactivate button').innerHTML = 'Envoyer';

    document.querySelector('div#formactivate button').onclick = function( e )
    {
        e.preventDefault();

        var request = new XMLHttpRequest();

        request.open( 'GET', url + 'api/users/activate/' + userDatas.token + '/' + userDatas.id + '/' + userDatas.email, true );

        request.onload = function() 
        {
            if( request.status >= 200 && request.status < 400 )
            {
                var validDatas = JSON.parse( request.responseText );

                if( validDatas.process === 'success' )
                {
                    location.hash = 'home';
                }
                else if( validDatas.proccess === 'fail' )
                {
                    if( validDatas.errors.tokenfail ||  validDatas.errors.urladdresserror )
                    {
                        document.querySelector( 'div#login div#error-connect' ).innerHTML = 'Un problème de connexion s\'est malheureusement produit.<br> Nous t\'invitons à te connecter plus tard.';
                    }
                }
            }
            else
            {
                document.querySelector( 'div#login div#error-connect' ).innerHTML = 'Un problème de connexion semble se poser. Tente de nouveau lorsque tu auras accès à Internet.';
            }
        };
        
        request.onerror = function()
        {
            document.querySelector( 'div#login div#error-connect' ).innerHTML = 'Un problème de connexion semble se poser. Tente de nouveau lorsque tu auras accès à Internet.';
        };

        request.send();
    };
};

/**
 * Hides all labels tag in the form <label>
 * @returns {void}
 */
var hideLabels = function()
{
    var labels = document.querySelectorAll( 'label' );
    Array.prototype.forEach.call( labels, function( label )
    {
        label.style.display = 'none';
    });
};

/**
 * Hides all link tag displays as button <a class="btn">
 * @returns {undefined}
 */
var hideBtns = function()
{
    var btns = document.querySelectorAll( 'a.btn' );
    Array.prototype.forEach.call( btns, function( btn )
    {
        btn.style.display = 'none';
    });
};

/**
 * Defines the message to display for the login or subscribe process when it fails.
 * @param {string} formType 'login' or 'subscribe'
 * @param {object} userDatas  Datas sent back through the API (contains errors also)
 * @returns {void}
 */
var formMsg = function( formType, userDatas )
{
    var msg = '';
    var callBack = '';
    if( formType === 'login' )
    { 
        if( userDatas.errors && userDatas.errors.accountNotActivated )
        {
            hideLabels();
            
            hideBtns();
                
            msg += 'Ton compte existe mais il n\'a pas été activé.<br>Souhaites-tu qu\'un message d\'activation te soit de nouveau envoyé?<br><div id="formactivate"></div>';   
            
            callBack = 'setActiveAccount';
        }
        else if( userDatas.errors && userDatas.errors.accountNotFound )
        {                
            msg += 'La connexion n\'a pas pu se faire. <br>Vérifie bien tes accès.';   
        }
        else
        {
            msg += 'La connexion n\'a pas pu se faire. <br>Essaie de te connecter à nouveau.';
        }
    }
    else if( formType === 'subscribe' )
    {
        if( userDatas.errors && userDatas.errors.accountNotActivated )
        {
            hideLabels();
            
            hideBtns();
                
            msg += 'Ton compte a été créé.<br>Un message d\'activation t\'a été envoyé par e-mail. Il te faut maintenant activer ton compte.<div id="formactivate"></div>';   
            
            callBack = 'setActiveAccount';
        }
        if( userDatas.errors.EmailUserFormat )
        {
            msg += 'Il semble que l\'adresse e-mail indiquée n\'a pas le bon format.<br>';
        }
        if( userDatas.errors.EmailUserExist )
        {
            msg += 'Tu dispose déjà d\'un compte. Tu peux redéfinir un mot de passe depuis le <a href="http://vitabulle.emploilausanne.ch">site Vitabulle</a> si tu as perdu le précédent.<br> ';
        }
        if( userDatas.errors.PassUserlong )
        {
            msg += 'Le mot de passe que tu proposes doit contenir au moins 8 caractères.';
        }
    } 
        
    document.querySelector( 'div#' + formType + ' div#error-connect' ).innerHTML = msg;
    
    if( callBack.length > 0 )
    {
        window[ callBack ]( userDatas );
    }
};

/**
 * Check forms fields and engage the sending of datas of the form to the API
 * @param {string} formType  'login' or 'subscribe'
 * @returns {void}
 */
var formDatasSend = function( formType )
{
    var timestamp   = Math.round(+new Date()/1000);
    
    var EmailUser   = document.querySelector( 'input#EmailUser' ).value;
    if( EmailUser.length === 0 )
    {
        EmailUser = 'empty'; // Needed for the API
        document.querySelector( 'input#EmailUser' ).classList.add( 'field-error' );
        document.querySelector( 'input#EmailUser + div' ).classList.remove( 'close' );
        document.querySelector( 'input#EmailUser + div' ).classList.add( 'open' );
    }
    else
    {
       EmailUser;
        document.querySelector( 'input#EmailUser' ).classList.remove( 'field-error' );
        document.querySelector( 'input#EmailUser + div' ).classList.remove( 'open' );
        document.querySelector( 'input#EmailUser + div' ).classList.add( 'close' );
    } 
    
    var PassUser    = document.querySelector( 'input#PassUser' ).value;
    if( PassUser.length === 0 )
    {
        PassUser = 'empty'; // Needed for the API
        document.querySelector( 'input#PassUser' ).classList.add( 'field-error' );
        document.querySelector( 'input#PassUser + div' ).classList.remove( 'close' );
        document.querySelector( 'input#PassUser + div' ).classList.add( 'open' );
    }
    else
    {
        PassUser;
        document.querySelector( 'input#PassUser' ).classList.remove( 'field-error' );
        document.querySelector( 'input#PassUser + div' ).classList.remove( 'open' );
        document.querySelector( 'input#PassUser + div' ).classList.add( 'close' );
    } 
    
    if( formType === 'login' )
    {
        var jsonUrl = url + 'api/users/login/' + timestamp + '/' + EmailUser + '/' + PassUser;
    }
    else if( formType === 'subscribe' )
    {
        var jsonUrl = url + 'api/users/subscribe/' + timestamp + '/' + EmailUser + '/' + PassUser;
    }
    
    var request = new XMLHttpRequest();

    request.open( 'GET', jsonUrl, true );

    request.onload = function() 
    {
        if( request.status >= 200 && request.status < 400 )
        {
            var userDatas = JSON.parse( request.responseText );
            
            if( userDatas.process === 'success' )
            {
                if( formType === 'login' )
                {
                    loguedin( userDatas );
                }
                else if( formType === 'subscribe' )
                {
                    hideLabels();

                    hideBtns();
                    document.querySelector( 'div#success-connect' ).classList.add( 'open' );
                    document.querySelector( 'div#success-connect' ).classList.remove( 'close' );
                }
            }
            else
            {
                if( !document.querySelector( 'div#error-connect' ).classList.contains( 'open' ) )
                {
                    formMsg( formType, userDatas ); 
                    document.querySelector( 'div#error-connect' ).classList.add( 'open' );
                    document.querySelector( 'div#error-connect' ).classList.remove( 'close' );
                }
                else
                {
                    document.querySelector( 'div#error-connect' ).classList.add( 'opacitynone' );
                    setTimeout( function()
                    {
                        formMsg( formType, userDatas );
                        document.querySelector( 'div#error-connect' ).classList.remove( 'opacitynone' );
                    }, 500);
                }
                document.querySelector( 'input#PassUser' ).value = '';
            }
        }
        else
        {
            
        }
    };
    
    request.onerror = function()
    {
        document.querySelector( 'div#error-connect' ).classList.add( 'open' );
        document.querySelector( 'div#error-connect' ).classList.remove( 'close' );
        document.querySelector( 'input#PassUser' ).value = '';
        
        document.querySelector( 'div#' + formType + ' div#error-connect' ).innerHTML = 'Un problème de connexion semble se poser. Vérifie ta connexion à Internet.';
    };

    request.send();    
};




    var errorsMsg = document.querySelectorAll( '.error' );

    if( errorsMsg && errorsMsg.length > 0 )
    {
        Array.prototype.forEach.call( errorsMsg, function( errorMsg )
        {
            errorMsg.style.display = 'none';
        });
    }

    if( document.querySelector( '.modal-call' ) )
    {
        document.querySelector( '.modal-call' ).onclick = function( e )
        {
            e.preventDefault();
            document.querySelector( 'div.modal' ).classList.add( 'drop' ); 
        };
    }

    if( document.querySelector( 'div.modal div i' ) )
    {
        document.querySelector( 'div.modal div i' ).onclick = function()
        {
            document.querySelector( 'div.modal' ).classList.remove( 'drop' );
        };
    }


    var formFields = document.querySelectorAll( 'input[type="text"], input[type="password"], input[type="email"], textarea' );

    if( formFields && formFields.length > 0 )
    {
        Array.prototype.forEach.call( formFields, function( formField )
        {
            formField.addEventListener('focus', function()
            {
                var prevElement = formField.previousElementSibling;
                
                if( prevElement && prevElement.tagName.toLowerCase() === 'span' )
                {
                    prevElement.classList.add( 'upperlabel' );
                }
            }, true);
        });
    }


    var formLogFields = document.querySelectorAll( 'div#login input#PassUser, div#login input#EmailUser' );

    setEventsOnFields( formLogFields, 'login' );

    var formSubFields = document.querySelectorAll( 'div#subscribe input#PassUser, div#subscribe input#EmailUser' );

    setEventsOnFields( formSubFields, 'subscribe' );
    

    if( document.querySelector( 'a[data-action="login"]' ) )
    {
        document.querySelector( 'a[data-action="login"]' ).onclick = function( e )
        {
            e.preventDefault();

            formDatasSend( 'login' );

        };
    }

    if( document.querySelector( 'a[data-action="subscribe"]' ) )
    {
        document.querySelector( 'a[data-action="subscribe"]' ).onclick = function( e )
        {
            e.preventDefault();

            formDatasSend( 'subscribe' );
        };
    }
